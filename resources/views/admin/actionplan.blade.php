@extends('admin.layout.layout')
@section('content')
    <div class="row">

        <div class="modal fade" id="modalPersonil"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Personil</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal"  >

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Personil</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="newperonil" class="form-control" id="personil" placeholder="Add New Personil" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPersonil" class="col-sm-2 control-label">List Personil</label>

                                    <div class="col-sm-6">
                                        <select name="Personil" class="form-control personil" id="inputPersonil" >
                                         @foreach($personils as $person)
                                            <option value="{!! $person['Personil'] !!}">{!! $person['Personil'] !!}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                    <div class="col-sm-2">
                                    <button data-id='inputPersonil'  class="btn btn-danger btn-block hapus deletemenu"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button data-id='personil' id="gsave" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalUnit"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Type/unit</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal"  >

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="unit" class="col-sm-2 control-label">Type/Unit</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="newperonil" class="form-control" id="unit" placeholder="Add New Type/unit" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPersonil" class="col-sm-2 control-label">List Type/Unit</label>

                                    <div class="col-sm-6">
                                        <select name="Personil" class="form-control unit" id="inputUnit" >
                                         @foreach($unit as $units)
                                            <option value="{!! $units['Unit'] !!}">{!! $units['Unit'] !!}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                    <div class="col-sm-2">
                                    <button data-id='inputUnit'  class="btn btn-danger btn-block deletemenu"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button data-id='unit'  id="gsave" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalSRI"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Safety Risk Indeks</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal"  >

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="sri" class="col-sm-2 control-label">Safety Risk Indeks</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="newperonil" class="form-control" id="sri" placeholder="Add Safety Risk Indeks" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sri" class="col-sm-2 control-label">Color</label>
                                    <div class="col-sm-6">
                                    <select class="form-control colorSelect" onclick="getval(this);">
                                      <option class="form-control" value="green">Green</option>
                                      <option class="form-control" value="yellow">Yellow</option>
                                      <option class="form-control" value="red">Red</option>
                                    </select>
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" id="colorSel" class="form-control greenColor" name="" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPersonil" class="col-sm-2 control-label">List Safety Risk Indeks</label>

                                    <div class="col-sm-6">
                                        <select name="Personil" class="form-control sri" id="inputSRI" >
                                         @foreach($sri as $sris)
                                            <option value="{!! $sris['SRI'] !!}">{!! $sris['SRI'] !!}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                    <div class="col-sm-2">
                                    <button data-id="inputSRI"  class="btn btn-danger btn-block deletemenu"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button data-id='sri' id="gsave" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalmaction"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update Mitigation Action</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal"  >

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputMaction" class="col-sm-2 control-label">Mitigation Action</label>

                                    <div class="col-sm-10">
                                      <textarea class="form-control" rows="8" name="MAction" id="inputMaction" placeholder="Mitigation Action. . ."></textarea>
                                 </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button data-id=' {!! Request::route('id') !!} ' id="gsave" class="btn btn-primary pull-right simpanmaction"><i class="fa fa-floppy-o"></i> SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="form-utama" class="col-md-12 sw-id sw-show">
            @if(Auth::user()->level)
            @if(!Request::route('id') && !Request::route('filter'))
            <a  class="btn btn-bitbucket btn-flat margin btn-sm " href="{{ url('actionplan/insert') }}">
                <i class="fa fa-plus"></i> Add Action Plan
            </a>
             <a class="btn btn-bitbucket btn-flat margin btn-sm" data-toggle="modal" data-target="#modalUnit">
                 <i class="fa fa-book"></i> Add Type/Unit
             </a>
             <a  class="btn btn-bitbucket btn-flat margin btn-sm " data-toggle="modal" data-target="#modalPersonil">
                <i class="fa fa-book"></i> Add Personil
            </a>
             <a class="btn btn-bitbucket btn-flat margin btn-sm" data-toggle="modal" data-target="#modalSRI">
                 <i class="fa fa-book"></i> Add Safety Risk Indeks
             </a>

            @endif
            @endif
            <!-- Horizontal Form
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search By No">
                        <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                    </div>
                </form>-->

            @foreach($data as $lead)
            <div class="box box-primary">
              <div class="box-body table-responsive no-padding">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style=" vertical-align: middle;" rowspan="2">No</th>
                        <th style=" vertical-align: middle;" colspan="2">Kejadian</th>
                        <th style=" vertical-align: middle;" rowspan="2">Personil</th>
                        <th style=" vertical-align: middle;" rowspan="2">Description</th>
                        <th style=" vertical-align: middle;" rowspan="2">Safety<br>Risk<br>Index</th>
                        <th style=" vertical-align: middle;" rowspan="2">Finding</th>
                        <th style=" vertical-align: middle;" rowspan="2">Recomendation</th>
                        <th style=" vertical-align: middle;" rowspan="2">PIC</th>
                        <th style=" vertical-align: middle;" rowspan="2">Time<br>line</th>
                        <th style=" vertical-align: middle;" rowspan="2">Mitigation Action</th>
                        <th style=" vertical-align: middle;" rowspan="2">Current<br>Status</th>
                        <th style=" vertical-align: middle;" rowspan="2">Safety<br>Risk <br>Index</th>
                    </tr>
                    <tr>
                        <th width="1%">Tgl<br>/<br>Waktu</th>
                        <th>Type<br>/<br>Unit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{!! $lead['Nomer'] !!}</td>
                        <td>{!! $lead['Tanggal'] !!}</td>
                        <td>{!! $lead['Unit'] !!}</td>
                        <td>{!! $lead['Personil'] !!}</td>
                        <td>{!! $lead['Description'] !!}</td>
                        <td><strong>{!! $lead['SRI1'] !!}</strong></td>
                        <td>{!! $lead['Finding'] !!}</td>
                        <td>{!! $lead['Recomendation'] !!}</td>
                        <td>{!! $lead['PIC'] !!}</td>
                        <td>{!! $lead['Timeline'] !!}</td>
                        <td>{!! $lead['M_Action'] !!}</td>
                        <td>{!! $lead['C_Status'] !!}</td>
                        <td>{!! $lead['SRI2'] !!}</td>

                    </tr>
                    </tbody>
                </table>
              </div>
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin">
                        @if(!Request::route('filter') && !Request::route('id'))
                         <li><button data-id="{!! $lead['id'] !!}" class="btn btn-success btn-sm viewmaction">View Action Plan</button></li>
                         @elseif(Request::route('filter'))
                         <li><button data-id="{!! $lead['id'] !!}" class="btn btn-success btn-sm viewmaction">View Action Plan</button></li>
                         @endif
                        @if(Auth::user()->level)
                        <li><button data-id="{!! $lead['id'] !!}"  class="btn btn-danger btn-sm deleteplan">Delete</button></li>
                         <li><button data-id="{!! $lead['id'] !!}" class="btn btn-success btn-sm editData">Edit</button></li>
                        @else
                        @if(Request::route('id'))
                        <li><button class="btn btn-success btn-sm addmaction" data-toggle="modal" data-target="#modalmaction">Update Mitigation</button></li>
                        @endif
                        @endif

                    </ul>
                    <ul class="pagination pagination-sm no-margin pull-right">
                    <p id="update">Last Update by {!! $lead['UpdateBy'] !!}</p>
                    </ul>

                </div>
            </div>

        @endforeach
        <div class="pagination pagination-sm no-margin pull-right">

        {{ $data->render() }}


        </div>

        </div>

    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">
    <style>

      .greenColor{
          background-color: #33CC33;
          color: #33CC33;
      }
      .redColor{
          background-color: #E60000;
          color: #E60000;
      }
      .yellowColor{
          background-color: #FFFF00;
          color: #FFFF00;
      }
    </style>
    <style>
      .table th  {
          text-align: center;
          vertical-align: middle;
      }
      .table td  {
          word-wrap: break-word;
          white-space:pre-line;
      }

    </style>
    @endpush
    @push('javascript')
        <script src="{{asset('dist/js/swichers.js')}}"></script>
        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>
    <script>
        new swicher().exe();
    </script>
    <script>
    function getval(sel)
    {
      switch(sel.value){
        case "green":
        document.getElementById("colorSel").className = "form-control greenColor";
        document.getElementById("colorSel").value = "success";
        break;
        case "yellow":
        document.getElementById("colorSel").className = "form-control yellowColor";
        document.getElementById("colorSel").value = "warning";
        break;
        case "red":
        document.getElementById("colorSel").className = "form-control redColor";
        document.getElementById("colorSel").value = "danger";
        break;
      }
    }
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
    <script>
        //Date Picket
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
</script>
<script type="text/javascript">
        //POST DATA
         $(document).on("click",'.simpan',function (e) {
              e.preventDefault();
              var fungsi = $(this).attr("data-id");
              var datafungsi = $("#"+fungsi).val();
              var color = $("#colorSel").val();

               if(!datafungsi.trim()==''){
                 swal({
                      title: "Simpan",
                      text: "Simpan Perubahan",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/insertData') }}",
                                data: {
                                    'fungsi'    : fungsi,
                                    'data'      : datafungsi,
                                    'color'     : color
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        location.reload();
                                    }else{
                                        infoalert('Information','Nomer Sudah Digunakan','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });
                }else{
                    infoalert('Information','Inputan Masih Kosong','error');
                }
            });

    </script>
    <script type="text/javascript">
        $(document).on("click",'.viewmaction',function (e) {
                e.preventDefault();
                var idview = $(this).attr("data-id");
                window.location = "{{ url('actionplan/view') }}" +'/'+idview;
            });
        $(document).on("click",'.editData',function (e) {
                e.preventDefault();
                var idview = $(this).attr("data-id");
                window.location = "{{ url('actionplan/editData') }}" +'/'+idview;
            });
    </script>
    <script type="text/javascript">
        $(document).on("click",'.deleteplan',function (e) {
                e.preventDefault();
                var idplan = $(this).attr("data-id");
                swal({
                      title: "Konfirmasi",
                      text: "Hapus Action Plan",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/deletePlan') }}",
                                data: {
                                    'idplan'    : idplan
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        location.reload();
                                    }else{
                                        infoalert('Information','Nomer Sudah Digunakan','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });
            });
    </script>
    <script type="text/javascript">
    $(document).on("click",'.simpanmaction',function (e) {
                e.preventDefault();
                var idaction = $(this).attr("data-id");
                var dataaction = $('#inputMaction').val();
                 swal({
                      title: "Update",
                      text: "Update Mitigation Action?",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/updatemaction') }}",
                                data: {
                                    'idaction'      : idaction,
                                    'dataaction'    : dataaction
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        location.reload();
                                    }else{
                                        infoalert('Information','Data Gagal Di input','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });

    });
    </script>
    <script type="text/javascript">
        $(document).on("click",'.deletemenu',function (e) {
                e.preventDefault();
                var idplan = $(this).attr("data-id");
                var dataindex = $('#'+idplan).val();
                swal({
                      title: "Konfirmasi",
                      text: "Hapus Data "+dataindex,
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/deleteoption') }}",
                                data: {
                                    'methoddelete'      : idplan,
                                    'datadelete'        : dataindex
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Hapus','success');
                                        location.reload();
                                    }else{
                                        infoalert('Information','Gagal Hapus','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });
            });
    </script>
    @endpush
@stop
