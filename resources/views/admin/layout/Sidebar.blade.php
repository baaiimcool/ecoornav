<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->


      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="{{ Request::is('dashboard') ? 'active' : '' }}{{ Request::is('/') ? 'active' : '' }}" ><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        @if(Auth::user()->level)
        <li class="{{ Request::is('managedashboard*') ? 'active' : '' }}" ><a href="{{ url('managedashboard') }}"><i class="fa fa-wrench"></i> <span>Manage Dashboard</span></a></li>
        @endif

        <li class="{{ Request::is('actionplan*') ? 'active' : '' }}"  ><a href="{{ url('actionplan') }}"><i class="fa fa-list-alt"></i> <span>Action Plan</span></a></li>
        @if(Auth::user()->level)
        <li class="{{ Request::is('hazard*') ? 'treeview active' : '' }}">
          <a href="#"><i class="fa fa-file-text-o"></i> <span>Hazard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('hazard/AddHazard') }}">New Hazard</a></li>
            <li><a href="{{ url('hazard/ViewHazard') }}">ViewHazard</a></li>
            <li><a href="{{ url('hazard/apptower') }}">APP/TOWER</a></li>
            <li><a href="{{ url('hazard/fss') }}">FSS</a></li>
            <li><a href="{{ url('hazard/ais') }}">AIS</a></li>
            <li><a href="{{ url('hazard/security') }}">SECURITY</a></li>
            <li><a href="{{ url('hazard/others') }}">OTHERS</a></li>
          </ul>
        </li>
        @endif
        <li class="{{ Request::is('safety*') ? 'active' : '' }}"><a href="{!! url('safety') !!}"><i class="fa fa-warning"></i> <span>Safety Alert</span></a></li>
        <li class="{{ Request::is('viewdokumen') ? 'active' : '' }}{{ Request::is('dokumen*') ? 'active' : '' }}"><a href="{!! url('viewdokumen') !!}"><i class="fa fa-files-o"></i> <span>Document</span></a></li>
        @if(Auth::check())
        <li class="{{ Request::is('adduser') ? 'treeview active' : '' }}{{ Request::is('admin-setting/profile') ? 'treeview active' : '' }}">
          <a href="#"><i class="fa fa-users"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(Auth::user()->level)
            <li><a href="{{ url('adduser') }}">Manage Users</a></li>
            @endif
            <li><a href="{{url('admin-setting/profile')}}">Edit User</a></li>
          </ul>
        </li>
        @endif

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>