@extends('admin.layout.layout')
@section('content')

  <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
            <i class="fa fa-calendar-o"></i>
              <h3 class="box-title">Topic : {{ $tanggal }} </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			@forelse($Topic as $data)
            <div class="row">
            <div class="col-xs-6">
                  <div class="progress-group">
                    <span class="progress-text">BOS</span>
                    <span class="progress-number"><b>{{ $data['BOS'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $data['BOS'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">BOC</span>
                    <span class="progress-number"><b>{{ $data['BOC'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $data['BOC'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Inconvinience</span>
                    <span class="progress-number"><b>{{ $data['Inconvinience'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $data['Inconvinience'] }}%"></div>
                    </div>
                  </div>
                   <div class="progress-group">
                    <span class="progress-text">Holding </span>
                    <span class="progress-number"><b>{{ $data['Holding'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width:{{ $data['Holding'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Go Around</span>
                    <span class="progress-number"><b>{{ $data['GoAround'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: {{ $data['GoAround'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  </div>
            	<div class="col-xs-6">
                  <div class="progress-group">
                    <span class="progress-text">Kites</span>
                    <span class="progress-number"><b>{{ $data['Kites'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-blue" style="width: {{ $data['Kites'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Laser</span>
                    <span class="progress-number"><b>{{ $data['Laser'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-blue" style="width: {{ $data['Laser'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Wild Life Hazard</span>
                    <span class="progress-number"><b>{{ $data['WildLife'] }}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-blue" style="width: {{ $data['WildLife'] }}%"></div>
                    </div>
                  </div>

                  </div></div>
			@empty
			<div class="row">
            <div class="col-xs-6">
                  <div class="progress-group">
                    <span class="progress-text">BOS</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">BOC</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Inconvinience</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                   <div class="progress-group">
                    <span class="progress-text">Holding </span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width:0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Go Around</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  </div>
            	<div class="col-xs-6">
                  <div class="progress-group">
                    <span class="progress-text">Kites</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Laser</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Wild Life Hazard</span>
                    <span class="progress-number"><b>0</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>

                  </div></div>
			@endforelse

            </div>
           <!-- /.box-body -->

                <div class="box-footer clearfix">
                    <ul class="pagination no-margin">
                    	<li><a class="btn bg-olive btn-flat btn-sm" href="{{ url('managedashboard/insert') }}"><i class="fa fa-plus"></i> Insert Topic</a></li>
                     	<li><a  class="btn bg-olive btn-flat btn-sm" id="filterTanggal"><i class="fa fa-tags"></i> Filter Tanggal</a></li>
                         
                        <li><a   class="btn bg-olive btn-flat btn-sm"><i class="fa fa-floppy-o"></i> Save Chart</a></li>
                    </ul>


                </div>

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


@push('style')

    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
 	<style>
            .table th  {
                text-align: center;
            }
            .table td  {
                text-align: justify;
                word-wrap: break-word;
                overflow-x:auto;
            }
            .bluerow
            {
                background-color:#83c9f7;color:#000000;
            }
        </style>
@endpush
 @push('javascript')

        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.id.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        //Date Picket
        $('#filterTanggal').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'id',
        })
        .on('changeDate', function(ev){
            @if(Request::route('tanggal'))
            window.location.href = ev.format();
            @else
            window.location.href = " {!! Request::url() !!}/" + ev.format();
            @endif
        });
</script>
@endpush

@stop