@extends('admin.layout.layout')
@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <a class="btn btn-bitbucket btn-flat margin btn-sm" href="{{ url('actionplan') }}">
                <i class="fa fa-arrow-left"></i> Back</a>
            <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Add Action Plan</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            @if(Route::currentRouteAction() == "App\Http\Controllers\admin\Plan@updateData")
            @foreach($data as $datas)
            <form class="form-horizontal" id="InputAP"  >

                <div class="box-body">
                    <div id="nomber" class="form-group">
                        <label for="inputNo" class="col-sm-2 control-label">No</label>

                        <div class="col-sm-10">
                            <input readonly="" type="text" name="Nomer" value=" {!! $datas['Nomer'] !!} " class="form-control" id="inputNo"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName3" class="col-sm-2 control-label">Tanggal</label>

                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="Tanggal" value="{!! $datas['Tanggal'] !!}" class="form-control pull-right" id="datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUnit" class="col-sm-2 control-label">Type/Unit</label>

                        <div class="col-sm-10">
                            <select name="Unit" class="form-control" id="inputUnit" >
                                <option value="">Select Type/Unit</option>
                                @foreach($unit as $units)
                                     <option value="{!! $units['Unit'] !!}">{!! $units['Unit'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPersonil" class="col-sm-2 control-label">Personil</label>

                        <div class="col-sm-10">
                            <select name="Personil" class="form-control" id="inputPersonil" >
                            <option value="">Select Personil</option>
                                @foreach($personils as $person)
                                    <option value="{!! $person['Personil'] !!}">{!! $person['Personil'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDes" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <textarea name="Description" value="{!! $datas['Description'] !!}"  class="form-control" rows="3" id="inputDes" placeholder="Enter ...">{!! $datas['Description'] !!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSRI1" class="col-sm-2 control-label">Safety Risk Indeks</label>

                         <div class="col-sm-10">
                            <select name="SRI1" class="form-control" id="inputSRI1" >
                                <option value="">Select Safety Risk Indeks</option>
                                @foreach($sri as $sris)
                                    <option value="<span class='label label-{!! $sris['color'] !!}'>{!! $sris['SRI'] !!}</span>">{!! $sris['SRI'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputFinding" class="col-sm-2 control-label">Finding</label>

                        <div class="col-sm-10">
                            <textarea value="{!! $datas['Finding'] !!}" class="form-control" name="Finding" rows="3" id="inputFinding" placeholder="Enter ...">{!! $datas['Finding'] !!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputRec" class="col-sm-2 control-label">Recomendation</label>

                        <div class="col-sm-10">
                            <textarea value="{!! $datas['Recomendation'] !!}" class="form-control" rows="3" name="Recomendation" id="inputRec" placeholder="Enter ...">{!! $datas['Recomendation'] !!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPIC" class="col-sm-2 control-label">PIC</label>

                        <div class="col-sm-10">
                            <input value="{!! $datas['PIC'] !!}" type="text" name="PIC" class="form-control" id="inputUnit"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputLine" class="col-sm-2 control-label">Timeline</label>

                        <div class="col-sm-10">
                            <input value="{!! $datas['Timeline'] !!}" type="text" name="Timeline" class="form-control" id="inputLine"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputStat" class="col-sm-2 control-label">Current Status</label>

                        <div class="col-sm-10">
                            <input value="{!! $datas['C_Status'] !!}" type="text" name="C_Status" class="form-control" id="inputStat"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSRI2" class="col-sm-2 control-label">Safety Risk Indeks</label>

                        <div class="col-sm-10">
                            <select name="SRI2" class="form-control" id="inputSRI2" >
                                <option value="">Select Safety Risk Indeks</option>
                                @foreach($sri as $sris)
                                    <option value="{!! $sris['SRI'] !!}">{!! $sris['SRI'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button  id="planUpdate" class="btn btn-info pull-right">Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
            @endforeach
            @else
            <form class="form-horizontal" id="InputAP"  >

                <div class="box-body">
                    <div id="nomber" class="form-group">
                        <label for="inputNo" class="col-sm-2 control-label">No</label>

                        <div class="col-sm-10">
                            <input type="text" name="Nomer" class="form-control" id="inputNo"  >
                        </div>
                    </div>
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                    <div class="form-group">
                        <label for="inputName3" class="col-sm-2 control-label">Tanggal</label>

                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="Tanggal" class="form-control pull-right" id="datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUnit" class="col-sm-2 control-label">Type/Unit</label>

                        <div class="col-sm-10">
                            <select name="Unit" class="form-control" id="inputUnit" >
                                <option value="">Select Type/Uni</option>
                                @foreach($unit as $units)
                                     <option value="{!! $units['Unit'] !!}">{!! $units['Unit'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPersonil" class="col-sm-2 control-label">Personil</label>

                        <div class="col-sm-10">
                            <select name="Personil" class="form-control" id="inputPersonil" >
                            <option value="">Select Personil</option>
                                @foreach($personils as $person)
                                    <option value="{!! $person['Personil'] !!}">{!! $person['Personil'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDes" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <textarea name="Description" class="form-control" rows="5" id="inputDes" placeholder="Enter ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSRI1" class="col-sm-2 control-label">Safety Risk Indeks</label>

                         <div class="col-sm-10">
                            <select name="SRI1" class="form-control" id="inputSRI1" >
                                <option value="">Select Safety Risk Indeks</option>
                                @foreach($sri as $sris)
                                    <option value="<span class='label label-{!! $sris['color'] !!}'>{!! $sris['SRI'] !!}</span>">{!! $sris['SRI'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputFinding" class="col-sm-2 control-label">Finding</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" name="Finding" rows="5" id="inputFinding" placeholder="Enter ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputRec" class="col-sm-2 control-label">Recomendation</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" name="Recomendation" id="inputRec" placeholder="Enter ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPIC" class="col-sm-2 control-label">PIC</label>

                        <div class="col-sm-10">
                            <input type="text" name="PIC" class="form-control" id="inputUnit"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputLine" class="col-sm-2 control-label">Timeline</label>

                        <div class="col-sm-10">
                            <input type="text" name="Timeline" class="form-control" id="inputLine"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputStat" class="col-sm-2 control-label">Current Status</label>

                        <div class="col-sm-10">
                            <input type="text" name="C_Status" class="form-control" id="inputStat"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSRI2" class="col-sm-2 control-label">Safety Risk Indeks</label>

                        <div class="col-sm-10">
                            <select name="SRI2" class="form-control" id="inputSRI2" >
                                <option value="">Select Safety Risk Indeks</option>
                                @foreach($sri as $sris)
                                    <option value="<span class='label label-{!! $sris['color'] !!}'>{!! $sris['SRI'] !!}</span>">{!! $sris['SRI'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button  id="plansave" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
            @endif
        </div>
        </div>
    </div>
    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">
        <style>
            .table th  {
                text-align: center;
            }
            .table td  {
                text-align: justify;
                word-wrap: break-word;
                overflow-x:auto;
            }
        </style>
    @endpush
    @push('javascript')
        <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>
           <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        //Date Picket
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
</script>
<script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
<script>
        //POST DATA
        $('#plansave').click(function (e) {

            e.preventDefault();

            //alert("asdasd");
                if ($('#inputNo').val() == '') {
                    $('#nomber').addClass("has-error");
                    $('#inputNo').after("<span class='help-block'>Number Is Required</span>");

                } else {
                    $('#plansave').attr('disabled', 'disabled');
                        swal({
                          title: "Simpan Data",
                          text: "Simpan data action plan?",
                          type: "info",
                          showCancelButton: true,
                          closeOnConfirm: false,
                          showLoaderOnConfirm: true,
                        },
                        function(){

                            $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/AddPlan') }}",
                                data: $('form#InputAP').serialize(),
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        window.location.href = "{{ url('actionplan') }}";
                                    }else{
                                        infoalert('Information','Nomer Sudah Digunakan','error');
                                    }

                                },
                                complete: function () {
                                    $("#plansave").removeAttr('disabled');

                                }
                            });

                        });

            }
        });
        $('#planUpdate').click(function (e) {

            e.preventDefault();

            //alert("asdasd");
                if ($('#inputNo').val() == '') {
                    $('#nomber').addClass("has-error");
                    $('#inputNo').after("<span class='help-block'>Number Is Required</span>");

                } else {
                    $('#plansave').attr('disabled', 'disabled');
                        swal({
                          title: "Update Data",
                          text: "Update data action plan?",
                          type: "info",
                          showCancelButton: true,
                          closeOnConfirm: false,
                          showLoaderOnConfirm: true,
                        },
                        function(){

                            $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/UpdatePlan') }}",
                                data: $('form#InputAP').serialize(),
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Update','success');
                                        window.location.href = "{{ url('actionplan') }}";
                                    }else{
                                        infoalert('Information','Nomer Sudah Digunakan','error');
                                    }

                                },
                                complete: function () {
                                    $("#plansave").removeAttr('disabled');

                                }
                            });

                        });

            }
        });
    </script>
    @endpush
@stop
