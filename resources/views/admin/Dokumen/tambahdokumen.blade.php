@extends('admin.layout.layout')
@section('content')
     <section class="content">
     <div class="modal fade" id="modalTahun"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Tahun</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal"  >

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Tahun</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="Tahun" class="form-control" id="Tahun" placeholder="Tahun" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPersonil" class="col-sm-2 control-label">List Tahun</label>

                                    <div class="col-sm-6">
                                        <select name="Tahun" class="form-control personil" id="inputTahun" >
                                         @foreach($tahun as $Tahun)
                                            <option value="{!! $Tahun['Tahun'] !!}">{!! $Tahun['Tahun'] !!}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                    <div class="col-sm-2">
                                    <button   class="btn btn-success btn-sm hapus">Delete</button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button data-id='Tahun' id="gsave" class="btn btn-info pull-right simpan">SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
     </div>
     <div class="modal fade" id="modalKategori"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Kategori</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal"  >

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Kategori</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="Kategori" class="form-control" id="Kategori" placeholder="Kategori" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPersonil" class="col-sm-2 control-label">List Kategori</label>

                                    <div class="col-sm-6">
                                        <select name="Kategori" class="form-control Kategori" id="inputPKategori" >
                                         @foreach($kategori as $Kategori)
                                            <option value="{!! $Kategori['Kategori'] !!}">{!! $Kategori['Kategori'] !!}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                    <div class="col-sm-2">
                                    <button   class="btn btn-success btn-sm hapus">Delete</button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button data-id='Kategori' id="gsave" class="btn btn-info pull-right simpan">SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
     </div>

      <div class="row">
        <div class="col-md-3">
          <a href="../viewdokumen" class="btn btn-primary btn-block margin-bottom">Back to Document</a>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tahun</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><i class="fa fa-folder-o"></i> All <span class="label label-primary pull-right"></span></a></li>
                @foreach($tahun as $Tahun)
                <li><a href="#"><i class="fa fa-folder-o"></i></i>{!!$Tahun['Tahun']!!}</a></li>
                @endforeach
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Kategori</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
              @foreach($kategori as $Kategori)
                <li><a href="#"><i class="fa  fa-folder"></i>{!! $Kategori['Kategori'] !!}</a></li>
                @endforeach
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Document</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <a class="btn bg-olive btn-flat btn-sm" data-toggle="modal" data-target="#modalKategori">
                 <i class="fa fa-tags"></i> Add Kategori
             </a>
             <a class="btn bg-olive btn-flat btn-sm" data-toggle="modal" data-target="#modalTahun">
                 <i class="fa fa-tags"></i> Add Tahun
             </a>
            <hr />
            <form id="InputDoc" name="uploaddoc">
            <div class="form-group">
                  <label>Nama Dokumen</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Dokumen">
            </div>
            <div class="form-group">
                  <label>Tahun</label>
                  <select class="form-control" name="tahun">
                     @foreach($tahun as $Tahun)
                        <option value="{!! $Tahun['Tahun'] !!}">{!! $Tahun['Tahun'] !!}</option>
                     @endforeach
                  </select>
            </div>
            <div class="form-group">
                  <label>Kategori</label>
                  <select class="form-control" name="kategori">
                     @foreach($kategori as $Kategori)
                        <option value="{!! $Kategori['Kategori'] !!}">{!! $Kategori['Kategori'] !!}</option>
                     @endforeach
                  </select>
            </div>
            <div class="form-group">
                  <label>Tanggal Publikasi</label>
                  <input type="text" class="form-control" id="datepicker" name="tanggal" placeholder="datepicker">
            </div>
            <div class="form-group">
                <div class="btn btn-default btn-file ">
                  <i class="fa fa-paperclip"></i> Select Document
                  <input type="file" name="attachment" id="attachment" onchange="checkvalid()">
                </div>
            </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary upload"><i class="fa fa-floppy-o "></i> Save</button>
              <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
            </div>
            <!-- /.box-footer -->
            </form>
          </div>
          <!-- /. box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">

    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">

    @endpush
    @push('javascript')
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
    function checkvalid()
    {
      var ext = $('#attachment').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['pdf','doc','docx','ppt','pptx','xls','xlsx']) == -1) {
          alert('invalid extension!');
          $('#attachment').val('');
        }
    }
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript">
      $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
    </script>
    <script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
    <script> 
    $(document).on("click",'.upload',function (e) {
                e.preventDefault();
                var form = document.forms.namedItem("uploaddoc"); 
                var formdata = new FormData(form);

                 swal({
                      title: "Simpan",
                      text: "Simpan Dokumen?",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                              async: true,
                                type: "POST",
                                contentType: false,
                                dataType: "json",
                                url: "{{ url('http/post/uploadData') }}",
                                data: formdata, // high importance!
                                processData: false, // high importance!
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        window.location.href = "{{ url('viewdokumen') }}";
                                    }else{
                                        infoalert('Information','Gagal Input Data','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });             
            });
    </script>
    <script> 
    $(document).on("click",'.simpan',function (e) {
                e.preventDefault();
                var fungsi = $(this).attr("data-id");
               var datafungsi = $("#"+fungsi).val();
               if(!datafungsi.trim()==''){
                 swal({
                      title: "Simpan",
                      text: "Simpan Perubahan",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/post/postData') }}",
                                data: {
                                    'fungsi'    : fungsi,
                                    'data'      : datafungsi   
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        location.reload();
                                    }else{
                                        infoalert('Information','Gagal Input Data','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });
                }else{
                    infoalert('Information','Inputan Masih Kosong','error');
                }               
            });
    </script>
    @endpush
@stop
