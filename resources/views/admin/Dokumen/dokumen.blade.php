@extends('admin.layout.layout')
@section('content')
     <section class="content">
      <div class="row">
        <div class="col-md-3">
        @if(Auth::user()->level)
          <a href="dokumen/putdokumen" class="btn btn-primary btn-block margin-bottom">Add Ducument</a>
        @endif
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tahun</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
  <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="{{ Request::is('viewdokumen') ? 'active' : '' }}"><a href="{{ url("viewdokumen")}}"><i class="fa fa-folder-o"></i> All <span class="label label-primary pull-right">{{ $Document->total() }}</span></a></li>
                @foreach($tahun as $Tahun)
                <li class="{{ Request::is("dokumen/tahun/".$Tahun['Tahun'] ) ? 'active' : '' }}"><a href="{{ url("dokumen/tahun/".$Tahun['Tahun'] )}}"><i class="fa fa-folder-o"></i></i>{!!$Tahun['Tahun']!!}</a></li>
                @endforeach
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Kategori</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
              @foreach($kategori as $Kategori)
                <li class="{{ Request::is("dokumen/kategori/".$Kategori['Kategori']) ? 'active' : '' }}"><a href="{{ url("dokumen/kategori/".$Kategori['Kategori']) }}"><i class="fa  fa-folder"></i>{!! $Kategori['Kategori'] !!}</a></li>
                @endforeach
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">List Document</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Document">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  <div class="btn-group">
                    <a type="button" href="{!! $Document->previousPageUrl() !!}" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></a>
                    <a type="button" href="{!! $Document->nextPageUrl() !!}" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></a>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                <tr>
                  <th colspan="2">Nama Dokumen</th>
                  <th>Kategori</th>
                  <th>Publikasi</th>
                  <th><i class="fa fa-edit"></i></th>
                </tr>
                @foreach($Document as $dokumen)
                  <tr>
                    <td class="mailbox-star"><i class="fa fa-file-pdf-o"></i></td>
                    <td class="mailbox-name"><a target="_blank" href="{{ url("upload/dokumen/".$dokumen['Filedokumen']) }}">{!! $dokumen['Nama'] !!}</a></td>
                    <td class="mailbox-name">{!! $dokumen['Kategori'] !!}</td>
                    <td class="mailbox-date">{!! date("d-m-Y",strtotime($dokumen['Publikasi'])) !!}</td>
                    <td class="mailbox-attachment"><a data-id='{!! $dokumen['Filedokumen'] !!}' id="hapusfile" href="#"><i class="fa fa-trash text-red"></i></a></td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
     <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">
    @endpush
    @push('javascript')
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>

    <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
   <script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
    <script type="text/javascript">
    $(document).on("click",'#hapusfile',function (e) {
                e.preventDefault();
                var idfile = $(this).attr("data-id");
                 swal({
                      title: "Konfirmasi",
                      text: "Hapus File",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/post/deletefile') }}",
                                data: {
                                    'namafile'      : idfile,
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','File Sukes Dihapus','success');
                                        location.reload();                                       
                                    }else{
                                        infoalert('Information','Gagal Hapus Dokumen','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });

    });
    </script>
    @endpush
@stop
