@extends('admin.layout.layout')
@section('content')

    <div class="row">
        <div class="col-md-12 ">
            <a class="btn bg-olive btn-flat margin-bottom btn-sm" href="{{ url('managedashboard') }}">
               <i class="fa fa-arrow-left"></i> Back</a>
            <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Insert Topic</h3>
            </div>

            <form class="form-horizontal" id="InputTopic"  >

                <div class="box-body">

                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                    <div class="form-group">
                        <label for="inputName3" class="col-sm-2 control-label">Tanggal</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="Tanggal" class="form-control pull-right" id="datepicker" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputBOS" class="col-sm-2 control-label">BOS</label>
                        <div class="col-sm-9">
                            <input type="text" name="BOS" class="form-control" id="inputBOS" required="required" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputBOC" class="col-sm-2 control-label">BOC</label>
                        <div class="col-sm-9">
                            <input type="text" name="BOC" class="form-control" id="inputBOC"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputInconvinience" class="col-sm-2 control-label">Inconvinience</label>
                        <div class="col-sm-9">
                            <input type="text" name="Inconvinience" class="form-control" id="inputInconvinience"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputWild" class="col-sm-2 control-label">Wild Life Hazard</label>
                        <div class="col-sm-9">
                            <input type="text" name="WildLife" class="form-control" id="inputWildLife"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKites" class="col-sm-2 control-label">Kites</label>
                        <div class="col-sm-9">
                            <input type="text" name="Kites" class="form-control" id="inputKites"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputLaser" class="col-sm-2 control-label">Laser</label>
                        <div class="col-sm-9">
                            <input type="text" name="Laser" class="form-control" id="inputLaser"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputHolding" class="col-sm-2 control-label">Holding</label>
                        <div class="col-sm-9">
                            <input type="text" name="Holding" class="form-control" id="inputHolding"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputGoAround" class="col-sm-2 control-label">Go Around</label>
                        <div class="col-sm-9">
                            <input type="text" name="GoAround" class="form-control" id="inputGoAround"  >
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button  id="saveTopic" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
                <!-- /.box-footer -->
            </form>

     </div>
     </div>

    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">

    @endpush
    @push('javascript')
        <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>
           <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
	<script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
    <script>
        //Date Picket
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayBtn: "linked",
		    clearBtn: true,
		    todayHighlight: true
        });
	</script>
	<script>

        $('#saveTopic').click(function (e) {

            e.preventDefault();

            //alert("asdasd");
                if ($('#datepicker').val() == '') {
                    $('#datepicker').addClass("inputError");
                    infoalert('Warning','Tanggal Harus Diisi','error');

                } else {
                    $('#saveTopic').attr('disabled', 'disabled');
                        swal({
                          title: "Simpan Data",
                          text: "Simpan data topic?",
                          type: "info",
                          showCancelButton: true,
                          closeOnConfirm: false,
                          showLoaderOnConfirm: true,
                        },
                        function(){

                            $.ajax({
                                type: "POST",
                                url: "{{ url('http/post/topic') }}",
                                data: $('form#InputTopic').serialize(),
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        window.location.href = "{{ url('managedashboard') }}";
                                    }else{
                                        infoalert('Information','Topic pada tanggal tersebut sudah diinputkan','error');
                                    }

                                },
                                complete: function () {
                                    $("#saveTopic").removeAttr('disabled');

                                }
                            });

                        });

            	}
        });
	</script>
    @endpush
@stop