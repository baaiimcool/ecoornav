@extends('admin.layout.layout')
@section('content')

        <div class="box box-info">
        <form class="form-horizontal" id="InputHZ"  >
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <div class="box-body">
                 <div class="form-group">
                        <label for="inputOperasi" class="col-sm-2 control-label">Hazard Type</label>

                         <div class="col-sm-10">
                            <select name="Tipe" class="form-control" id="inputOperasi" >
                                    <option value="APPTOWER">APP/TOWER</option>
                                    <option value="FSS">FSS</option>
                                    <option value="AIS">AIS</option>
                                    <option value="SECURITY">SECURITY</option>
                                    <option value="OTHERS">OTHERS</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputOperasi" class="col-sm-2 control-label">Type / Operasi / Kegiatan</label>

                         <div class="col-sm-10">
                            <select name="Operasi" class="form-control" id="inputOperasi" >
                                    <option value="FASILITAS">FASILITAS</option>
                                    <option value="PROCEDURE">PROCEDURE</option>
                                    <option value="SDM">SDM</option>
                            </select>
                    </div>
                    </div>
                <div class="form-group">
                        <label for="inputHazardumum" class="col-sm-2 control-label">Hazard Umum</label>

                        <div class="col-sm-10">
                            <input name="Humum" class="form-control" id="inputHazardumum" placeholder="Hazard Umum">
                            <input type="Radio" value="black" name="color" checked="">Black
                            <input type="Radio" value="blue" name="color">Blue
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSKHazard" class="col-sm-2 control-label">Komponen Hazard</label>

                        <div class="col-sm-10">
                            <textarea name="SKHazard" class="form-control" rows="5" id="inputSKHazard" placeholder="Spesifik Komponen Hazard"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKHazard" class="col-sm-2 control-label">Konsekuensi Hazard</label>

                        <div class="col-sm-10">
                            <textarea name="KHazard" class="form-control" rows="5" id="inputKHazard" placeholder="Konsekuensi Hazard"></textarea>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="inputResiko" class="col-sm-2 control-label">Pengendalian risiko yang sudah dilakukan </label>

                        <div class="col-sm-10">
                            <textarea name="Resiko" class="form-control" rows="5" id="inputResiko" placeholder="Pengendalian risiko yang sudah dilakukan "></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSRI" class="col-sm-2 control-label">Safety Risk Indeks</label>

                         <div class="col-sm-10">
                            <select name="SRI" class="form-control" id="inputSRI" >
                                <option value="">Select Safety Risk Indeks</option>
                                @foreach($sri as $sris)
                                    <option value="<span class='label label-{!! $sris['color'] !!}'>{!! $sris['SRI'] !!}</span>">{!! $sris['SRI'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTlanjut" class="col-sm-2 control-label">Tindak Lanjut Resiko</label>

                        <div class="col-sm-10">
                            <textarea name="TindakLanjut" class="form-control" rows="5" placeholder="Tindak lanjut untuk mengurangi  risiko keselamatan dan hasil indeks risiko  keselamatan"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputName3" class="col-sm-2 control-label">Kondisi Pertanggal</label>

                        <div class="col-sm-10">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="Tanggal" class="form-control pull-right tanggal" id="datepicker">

                            </div>
                            <textarea name="KondisiT" class="form-control" rows="5" id="inputName3" placeholder="Kondisi Pertanggal Tanggal"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputCRI" class="col-sm-2 control-label">Current Risk Indeks</label>

                        <div class="col-sm-10">
                            <input type="text" name="CRI" class="form-control" id="inputCRI"  >
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button  id="HazardSave" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
            </div>

    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">
    @endpush
    @push('javascript')
        <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>
           <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        //Date Picket
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
</script>
 <script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
<script type="text/javascript">
    $('#HazardSave').click(function (e) {
            e.preventDefault();

                if ($('.tanggal').val() == '' || $('#inputHazardumum').val() == '') {
                    sweetAlert("Oops...", "Hazard Umum / Kondisi Tanggal Wajib Diisi", "error");
                } else {
                    $('#HazardSave').attr('disabled', 'disabled');
                        swal({
                          title: "Simpan Hazard",
                          text: "Simpan Hazard ?",
                          type: "info",
                          showCancelButton: true,
                          closeOnConfirm: false,
                          showLoaderOnConfirm: true,
                        },
                        function(){

                            $.ajax({
                                type: "POST",
                                url: "{{ url('http/auth/insertHazard') }}",
                                data: $('form#InputHZ').serialize(),
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                    }else{
                                        infoalert('Information','Nomer Sudah Digunakan','error');
                                    }

                                },
                                complete: function () {
                                    var link = $('.tanggal').val().format("YYYY-MM-DD");;

                                    var Operasi = $('#inputOperasi').val();
                                    $("#plansave").removeAttr('disabled');
                                    window.location.href = "{{ url('')  }}/hazard/ViewHazard/" +link;
                                    //location.reload;
                                }
                            });

                        });

            }
        });
</script>
    @endpush
@stop
