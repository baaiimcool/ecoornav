@extends('admin.layout.layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a  class="btn bg-olive btn-flat margin-left btn-sm" id="datepicker">
                <i class="fa fa-calendar-check-o"></i> Pilih Kondisi Bulan
            </a>
            <a href="{{ url('hazard/printHazard') }}/{{ Request::route('tanggal') }}" target="_blank" class="btn bg-olive btn-flat margin btn-sm">
                <i class="fa fa-print"></i> Print Hazard
            </a>
        </div>
    </div>
    @if(Request::route('tanggal'))

	 <div class="box box-primary">
        <table class="table table-bordered tg ">
          <thead>
          <tr>
            <th style=" vertical-align: middle;" >No</th>
            <th style=" vertical-align: middle;">Tipe / Operasi /<br> Kegiatan</th>
            <th style=" vertical-align: middle;">Hazard Umum</th>
            <th style=" vertical-align: middle;">Specifik Komponen Hazard</th>
            <th style=" vertical-align: middle;">Konsekuensi Hazard<br></th>
            <th style=" vertical-align: middle;">Pengendalian risiko yang sudah dilakukan</th>
            <th style=" vertical-align: middle;">Safety<br>Risk<br>Indek</th>
            <th style=" vertical-align: middle;">Tindak lanjut untuk mengurangi risiko keselamatan dan hasil indeks risiko keselamatan</th>
            <th style=" vertical-align: middle;">Kondisi Per 1-{{ Request::route('tanggal') }}</th>
            <th style=" vertical-align: middle;">Current<br>Risk<br>Indeks</th>
          </tr>
          </thead>

          <tr>
            <td class="bluerow" colspan="10"><strong>APP/TOWER</strong></td>
          </tr>

          <tr>
            <td class="bluerow"><strong>A</strong></td>
            <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
          </tr>

          @foreach($data as $item )
          @if( $item['Tipe']=="APPTOWER")
          @if( $item['Operasi']=="FASILITAS")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow"><strong>B</strong></td>
            <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
          </tr>
          @foreach($data as $item )
          @if( $item['Tipe']=="APPTOWER")
          @if( $item['Operasi']=="PROCEDURE")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow"><strong>C</strong></td>
            <td class="bluerow" colspan="9"><strong>SDM</strong></td>
          </tr>
           @foreach($data as $item )
          @if( $item['Tipe']=="APPTOWER")
          @if( $item['Operasi']=="SDM")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow" colspan="10"><strong>FSS</strong></td>
          </tr>

          <tr>
            <td class="bluerow"><strong>A</strong></td>
            <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
          </tr>
          <?php $no=1;?>
           @foreach($data as $item )
          @if( $item['Tipe']=="FSS")
          @if( $item['Operasi']=="FASILITAS")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow"><strong>B</strong></td>
            <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
          </tr>
            @foreach($data as $item )
          @if( $item['Tipe']=="FSS")
          @if( $item['Operasi']=="PROCEDURE")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          </tr>
          <tr>
            <td class="bluerow"><strong>C</strong></td>
            <td class="bluerow" colspan="9"><strong>SDM</strong></td>
          </tr>
            @foreach($data as $item )
          @if( $item['Tipe']=="FSS")
          @if( $item['Operasi']=="SDM")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow" colspan="10"><strong>AIS</strong></td>
          </tr>

          <tr>
            <td class="bluerow"><strong>A</strong></td>
            <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
          </tr>
          <?php $no=1;?>
            @foreach($data as $item )
          @if( $item['Tipe']=="AIS")
          @if( $item['Operasi']=="FASILITAS")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow"><strong>B</strong></td>
            <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
          </tr>
            @foreach($data as $item )
          @if( $item['Tipe']=="AIS")
          @if( $item['Operasi']=="PROCEDURE")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          </tr>
          <tr>
            <td class="bluerow"><strong>C</strong></td>
            <td class="bluerow" colspan="9"><strong>SDM</strong></td>
          </tr>
            @foreach($data as $item )
          @if( $item['Tipe']=="AIS")
          @if( $item['Operasi']=="SDM")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow" colspan="10"><strong>SECURITY</strong></td>
          </tr>

          <tr>
            <td class="bluerow"><strong>A</strong></td>
            <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
          </tr>
          <?php $no=1;?>
            @foreach($data as $item )
          @if( $item['Tipe']=="SECURITY")
          @if( $item['Operasi']=="FASILITAS")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow"><strong>B</strong></td>
            <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
          </tr>
          @foreach($data as $item )
          @if( $item['Tipe']=="SECURITY")
          @if( $item['Operasi']=="PROCEDURE")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          </tr>
          <tr>
            <td class="bluerow"><strong>C</strong></td>
            <td class="bluerow" colspan="9"><strong>SDM</strong></td>
          </tr>
            @foreach($data as $item )
          @if( $item['Tipe']=="SECURITY")
          @if( $item['Operasi']=="SDM")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow" colspan="10"><strong>OTHERS</strong></td>
          </tr>

          <tr>
            <td class="bluerow"><strong>A</strong></td>
            <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
          </tr>
          <?php $no=1;?>
            @foreach($data as $item )
          @if( $item['Tipe']=="OTHERS")
          @if( $item['Operasi']=="FASILITAS")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          <tr>
            <td class="bluerow"><strong>B</strong></td>
            <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
          </tr>
           @foreach($data as $item )
          @if( $item['Tipe']=="OTHERS")
          @if( $item['Operasi']=="PROCEDURE")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
          </tr>
          <tr>
            <td class="bluerow"><strong>C</strong></td>
            <td class="bluerow" colspan="9"><strong>SDM</strong></td>
          </tr>
            @foreach($data as $item )
          @if( $item['Tipe']=="OTHERS")
          @if( $item['Operasi']=="SDM")
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
            <td>{!! $item['KomponenHazard'] !!}</td>
            <td>{!! $item['KonsekuensiHazard'] !!}</td>
            <td>{!! $item['ResikoDilakukan'] !!}</td>
            <td>{!! $item['SRI'] !!}</td>
            <td>{!! $item['TindakLanjut'] !!}</td>
            <td>{!! $item['Kondisi'] !!}</td>
            <td>{!! $item['CRI'] !!}</td>
          </tr>
          @endif
          @endif
          @endforeach
        </table>
	</div>
    @endif
@push('style')

    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
 	<style>
            .table th  {
              text-align: center;
              vertical-align: middle;
            }
            .table td  {
              word-wrap: break-word;
              white-space:pre-line;
            }
            .bluerow
            {
                background-color:#9cf;color:#000;
                font-size: 13px;
            }
        </style>
@endpush
 @push('javascript')

        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.id.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        //Date Picket
        $('#datepicker').datepicker({
             viewMode: "months",
                minViewMode: "months",
            format: 'mm-yyyy',
            autoclose: true,
            language: 'id',
        })
        .on('changeDate', function(ev){
            @if(Request::route('tanggal'))
            window.location.href = ev.format();
            @else
            window.location.href = " {!! Request::url() !!}/" + ev.format();
            @endif
        });
</script>
@endpush
@stop
