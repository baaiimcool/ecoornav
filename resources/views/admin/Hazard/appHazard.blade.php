@extends('admin.layout.layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a  class="btn bg-olive btn-flat margin btn-sm" id="datepicker">
                <i class="fa fa-calendar-check-o"></i> Pilih Kondisi Bulan
            </a>
        </div>
    </div>
    @if(Request::route('tanggal'))
	 <div class="box box-info">
        <table class="table table-bordered tg ">
          <tr>
            <th style=" vertical-align: middle;" >No</th>
            <th style=" vertical-align: middle;">Tipe / Operasi /<br> Kegiatan</th>
            <th style=" vertical-align: middle;">Hazard Umum</th>
            <th style=" vertical-align: middle;">Specifik Komponen<br>Hazard</th>
            <th style=" vertical-align: middle;">Konsekuensi Hazard<br></th>
            <th style=" vertical-align: middle;">Pengendalian risiko yang sudah dilakukan</th>
            <th style=" vertical-align: middle;">Safety<br>Risk<br>Indek</th>
            <th style=" vertical-align: middle;">Tindak lanjut untuk mengurangi risiko keselamatan dan hasil indeks risiko keselamatan</th>
            <th style=" vertical-align: middle;">Kondisi Per 1-{{ Request::route('tanggal') }}</th>
            <th style=" vertical-align: middle;">Current<br>Risk<br>Indeks</th>
          </tr>
          
          <tr>
            <td class="bluerow"><strong>A</strong></td>
            <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
          </tr>
          @foreach($facility as $fasilitas)
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $fasilitas['HazardUmum'] !!}</strong></td>
            <td>{!! $fasilitas['KomponenHazard'] !!}</td>
            <td>{!! $fasilitas['KonsekuensiHazard'] !!}</td>
            <td>{!! $fasilitas['ResikoDilakukan'] !!}</td>
            <td>{!! $fasilitas['SRI'] !!}</td>
            <td>{!! $fasilitas['TindakLanjut'] !!}</td>
            <td>{!! $fasilitas['Kondisi'] !!}</td>
            <td>{!! $fasilitas['CRI'] !!}</td>
          </tr>
          @endforeach
          <tr>
            <td class="bluerow"><strong>B</strong></td>
            <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
          </tr>
          @foreach($prosedur as $PROCEDURE)
          <tr>
            <td> {!! $no++ !!} </td>
            <td></td>
            <td><strong>{!! $PROCEDURE['HazardUmum'] !!}</strong></td>
            <td>{!! $PROCEDURE['KomponenHazard'] !!}</td>
            <td>{!! $PROCEDURE['KonsekuensiHazard'] !!}</td>
            <td>{!! $PROCEDURE['ResikoDilakukan'] !!}</td>
            <td>{!! $PROCEDURE['SRI'] !!}</td>
            <td>{!! $PROCEDURE['TindakLanjut'] !!}</td>
            <td>{!! $PROCEDURE['Kondisi'] !!}</td>
            <td>{!! $PROCEDURE['CRI'] !!}</td>
          </tr>
          @endforeach</td>
          </tr>
          <tr>
            <td class="bluerow"><strong>C</strong></td>
            <td class="bluerow" colspan="9"><strong>SDM</strong></td>
          </tr>
          @foreach($sdm as $sdma)
          <tr>
            <td> {!! $no++ !!}</td>
            <td></td>
            <td><strong>{!! $sdma['HazardUmum'] !!}</strong></td>
            <td>{!! $sdma['KomponenHazard'] !!}</td>
            <td>{!! $sdma['KonsekuensiHazard'] !!}</td>
            <td>{!! $sdma['ResikoDilakukan'] !!}</td>
            <td>{!! $sdma['SRI'] !!}</td>
            <td>{!! $sdma['TindakLanjut'] !!}</td>
            <td>{!! $sdma['Kondisi'] !!}</td>
            <td>{!! $sdma['CRI'] !!}</td>
          </tr>
          @endforeach
        </table>
	</div>
    @endif
@push('style')

    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
 	<style>
            .table th  {
                text-align: center;
            }
            .table td  {
                text-align: justify;
                word-wrap: break-word;
                overflow-x:auto;
            }
            .bluerow
            {
                background-color:#83c9f7;color:#000000;
            }
        </style>
@endpush
 @push('javascript')

        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('plugins/datepicker/bootstrap-datepicker.id.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        //Date Picket
        $('#datepicker').datepicker({
             viewMode: "months", 
                minViewMode: "months",
            format: 'mm-yyyy',
            autoclose: true,
            language: 'id',
        })
        .on('changeDate', function(ev){
            @if(Request::route('tanggal'))
            window.location.href = ev.format();
            @else
            window.location.href = " {!! Request::url() !!}/" + ev.format();
            @endif
        });
</script>
@endpush
@stop
