<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>e-Corrective Action Plan - Airnav Indonesia</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->

  <style>
            .table th  {
              text-align: center;
              vertical-align: middle;
              font-size: 10px;
            }
            .table td  {

            }
            .bluerow
            {
                background-color:#9cf;color:#000;
                font-size: 13px;
            }
        </style>
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.css')}}">

</head>
  <body>
    <div class="box box-primary">
         <table class="table table-bordered tg ">
           <thead>
           <tr>
             <th style=" vertical-align: middle;" >No</th>
             <th style=" vertical-align: middle;">Tipe / Operasi /<br> Kegiatan</th>
             <th style=" vertical-align: middle;">Hazard Umum</th>
             <th style=" vertical-align: middle;">Specifik Komponen Hazard</th>
             <th style=" vertical-align: middle;">Konsekuensi Hazard<br></th>
             <th style=" vertical-align: middle;">Pengendalian risiko yang sudah dilakukan</th>
             <th style=" vertical-align: middle;">Safety<br>Risk<br>Indek</th>
             <th style=" vertical-align: middle;">Tindak lanjut untuk mengurangi risiko keselamatan dan hasil indeks risiko keselamatan</th>
             <th style=" vertical-align: middle;">Kondisi Per 1-{{ Request::route('tanggal') }}</th>
             <th style=" vertical-align: middle;">Current<br>Risk<br>Indeks</th>
           </tr>
           </thead>

           <tr>
             <td class="bluerow" colspan="10"><strong>APP/TOWER</strong></td>
           </tr>

           <tr>
             <td class="bluerow"><strong>A</strong></td>
             <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
           </tr>

           @foreach($data as $item )
           @if( $item['Tipe']=="APPTOWER")
           @if( $item['Operasi']=="FASILITAS")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow"><strong>B</strong></td>
             <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
           </tr>
           @foreach($data as $item )
           @if( $item['Tipe']=="APPTOWER")
           @if( $item['Operasi']=="PROCEDURE")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow"><strong>C</strong></td>
             <td class="bluerow" colspan="9"><strong>SDM</strong></td>
           </tr>
            @foreach($data as $item )
           @if( $item['Tipe']=="APPTOWER")
           @if( $item['Operasi']=="SDM")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow" colspan="10"><strong>FSS</strong></td>
           </tr>

           <tr>
             <td class="bluerow"><strong>A</strong></td>
             <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
           </tr>
           <?php $no=1;?>
            @foreach($data as $item )
           @if( $item['Tipe']=="FSS")
           @if( $item['Operasi']=="FASILITAS")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow"><strong>B</strong></td>
             <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
           </tr>
             @foreach($data as $item )
           @if( $item['Tipe']=="FSS")
           @if( $item['Operasi']=="PROCEDURE")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           </tr>
           <tr>
             <td class="bluerow"><strong>C</strong></td>
             <td class="bluerow" colspan="9"><strong>SDM</strong></td>
           </tr>
             @foreach($data as $item )
           @if( $item['Tipe']=="FSS")
           @if( $item['Operasi']=="SDM")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow" colspan="10"><strong>AIS</strong></td>
           </tr>

           <tr>
             <td class="bluerow"><strong>A</strong></td>
             <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
           </tr>
           <?php $no=1;?>
             @foreach($data as $item )
           @if( $item['Tipe']=="AIS")
           @if( $item['Operasi']=="FASILITAS")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow"><strong>B</strong></td>
             <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
           </tr>
             @foreach($data as $item )
           @if( $item['Tipe']=="AIS")
           @if( $item['Operasi']=="PROCEDURE")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           </tr>
           <tr>
             <td class="bluerow"><strong>C</strong></td>
             <td class="bluerow" colspan="9"><strong>SDM</strong></td>
           </tr>
             @foreach($data as $item )
           @if( $item['Tipe']=="AIS")
           @if( $item['Operasi']=="SDM")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow" colspan="10"><strong>SECURITY</strong></td>
           </tr>

           <tr>
             <td class="bluerow"><strong>A</strong></td>
             <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
           </tr>
           <?php $no=1;?>
             @foreach($data as $item )
           @if( $item['Tipe']=="SECURITY")
           @if( $item['Operasi']=="FASILITAS")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow"><strong>B</strong></td>
             <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
           </tr>
           @foreach($data as $item )
           @if( $item['Tipe']=="SECURITY")
           @if( $item['Operasi']=="PROCEDURE")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           </tr>
           <tr>
             <td class="bluerow"><strong>C</strong></td>
             <td class="bluerow" colspan="9"><strong>SDM</strong></td>
           </tr>
             @foreach($data as $item )
           @if( $item['Tipe']=="SECURITY")
           @if( $item['Operasi']=="SDM")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow" colspan="10"><strong>OTHERS</strong></td>
           </tr>

           <tr>
             <td class="bluerow"><strong>A</strong></td>
             <td class="bluerow" colspan="9"><strong>FASILITAS</strong></td>
           </tr>
           <?php $no=1;?>
             @foreach($data as $item )
           @if( $item['Tipe']=="OTHERS")
           @if( $item['Operasi']=="FASILITAS")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           <tr>
             <td class="bluerow"><strong>B</strong></td>
             <td class="bluerow" colspan="9"><strong>PROCEDURE</strong></td>
           </tr>
            @foreach($data as $item )
           @if( $item['Tipe']=="OTHERS")
           @if( $item['Operasi']=="PROCEDURE")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
           </tr>
           <tr>
             <td class="bluerow"><strong>C</strong></td>
             <td class="bluerow" colspan="9"><strong>SDM</strong></td>
           </tr>
             @foreach($data as $item )
           @if( $item['Tipe']=="OTHERS")
           @if( $item['Operasi']=="SDM")
           <tr>
             <td> {!! $no++ !!} </td>
             <td></td>
             <td><strong>{!! $item['HazardUmum']  !!}</strong></td>
             <td>{!! $item['KomponenHazard'] !!}</td>
             <td>{!! $item['KonsekuensiHazard'] !!}</td>
             <td>{!! $item['ResikoDilakukan'] !!}</td>
             <td>{!! $item['SRI'] !!}</td>
             <td>{!! $item['TindakLanjut'] !!}</td>
             <td>{!! $item['Kondisi'] !!}</td>
             <td>{!! $item['CRI'] !!}</td>
           </tr>
           @endif
           @endif
           @endforeach
         </table>
 	</div>

  </body>
</html>
