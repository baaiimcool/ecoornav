@extends('admin.layout.layout')
@section('content')
  <section class="content">



    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">User profile</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <div class="box box-info">
                  <div class="panel-heading">User profile</div>
                  <div class="panel-body">
                    <form id="userpost" class="form-horizontal" method="post" action="{{url('admin-setting/profile/update')}}" app-data-method="get" app-data-url="{{url('admin-setting/profile/data')}}">
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="name" name="name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="email" name="email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="newpass" class="col-sm-3 control-label">New Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" name="newpass" id="newpass" placeholder="leave blank">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-default">SAVE</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- /.col -->
              <div id="gambar">
              <div class="col-md-4">
                <div class="box box-info">
                  <div class="panel-heading">photo upload</div>
                  <div class="panel-body">
                  
                    <img style="width: 200px; height: 250px" src="{{asset('upload/images/imgprofile_'.Auth::user()->id.'.jpg')}}" class="img-responsive" alt="Responsive image">
                    <br>
                    </div>
                          <div class="btn btn-default btn-file ">
                            <i class="fa fa-paperclip"></i> Pilih Photo
                            <input  type="file" name="uploadImg" data-url="{{url('admin-setting/profile/photo')}}" multiple >
                          </div>
                      </div> 
                  </div>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->

          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

    <!-- /.row -->

    @push('javascript')
    <script src="{{asset('js/pluginjQ.js')}}"></script>
    <script src="{{asset('uplugin/js/jquery.fileupload.js')}}"></script>
      <script>
        $('#userpost').ajaxOnLoad(function (res,status) {
          if(status=="success"){
            for(var key in res){
              $('input[name="'+key+'"]').val(res[key]);
            }
          }
        });
        $('#userpost').appSimpleSubmit(function (res,status) {
          if(status=="success"){
            for(var key in res){
              $('input[name="'+key+'"]').val(res[key]);
            }
          }
        });
        $(function () {
          $('input[name="uploadImg"]').fileupload({
            dataType: 'json',
            done: function (e, data) {
              $.each(data.result.files, function (index, file) {
              });
            },
              complete: function () {
               location.reload();
            }
          });
        });
      </script>
    @endpush
  </section>
@stop