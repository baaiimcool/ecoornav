@extends('admin.layout.layout')
@section('content')
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <section class="col-lg-8 connectedSortable">
           <div class="box box-primary">
            <div class="box-header with-border">

              <h3 class="box-title">Monthly Top Topic</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                <div class="btn-group">
                  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-floppy-o"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a>Save All Charts</a></li>
                    <li class="divider"></li>
                    <li><a href="#">BOS / BOC / Inconvinence</a></li>
                    <li><a href="#">Wild Life Hazard / Kites / Laser</a></li>
                    <li><a href="#">Holding / Go Around</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-primary btn-xs" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h5><strong>BBI (BOS / BOC / Inconvinence)</strong></h5>
                  <hr/>
                  <div class="chart">
                    <canvas id="cartBBI" style="height: 180px;"></canvas>
                  </div>
                  <h5><strong>Wild Life Hazard / Kites / Laser</strong></h5>
                  <hr/>
                  <div class="chart">
                    <canvas id="cartWKL" style="height: 180px;"></canvas>
                  </div>
                  <h5><strong>Holding / Go Around</strong></h5>
                  <hr/>
                  <div class="chart">
                    <canvas id="cartHGA" style="height: 180px;"></canvas>
                  </div>

                </div>

              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->

            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </section>

        <section class="col-lg-4 connectedSortable">
          <!-- Calendar -->
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">

              <h3 class="box-title">Daily Topic</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->

                <button type="button" class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-primary btn-xs" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="kalender"><div data-date="08/02/2017"></div></div>
            </div>
            <!-- /.box-body -->
            @forelse($Topic as $data)
            <div class="box-footer text-black">
                  <div class="progress-group">
                    <span class="progress-text">BOS </span>
                    <span class="progress-number"><b>{{ $data['BOS'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $data['BOS'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">BOC</span>
                    <span class="progress-number"><b>{{ $data['BOC'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $data['BOC'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Inconvinience</span>
                    <span class="progress-number"><b>{{ $data['Inconvinience'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $data['Inconvinience'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Wild Life Hazard</span>
                    <span class="progress-number"><b>{{ $data['WildLife'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-blue" style="width: {{ $data['WildLife'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Kites</span>
                    <span class="progress-number"><b>{{ $data['Kites'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-blue" style="width: {{ $data['Kites'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Laser</span>
                    <span class="progress-number"><b>{{ $data['Laser'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-blue" style="width: {{ $data['Laser'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Holding</span>
                    <span class="progress-number"><b>{{ $data['Holding'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: {{ $data['Holding'] }}%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Go Around</span>
                    <span class="progress-number"><b>{{ $data['GoAround'] }}</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: {{ $data['GoAround'] }}%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
            </div>
            @empty
            <div class="box-footer text-black">
                  <div class="progress-group">
                    <span class="progress-text">BOS </span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">BOC</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Inconvinience</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Wild Life Hazard</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Kites</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Laser</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Holding</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <div class="progress-group">
                    <span class="progress-text">Go Around</span>
                    <span class="progress-number"><b>0</b>/100</span>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
            </div>
            @endforelse
          </div>
          <!-- /.box -->
        </section>

        </div>
        <!-- /.col -->
      <!-- /.row -->


    </section>

     @push('style')

    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
    

    @endpush
    @push('javascript')


    <script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
    <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript">
      $('#kalender').datepicker({
          todayHighlight: true
      });
    </script>

    <script>
    $(function () {
      'use strict';
      var cartBBICanvas = $("#cartBBI").get(0).getContext("2d");

      var cartBBI = new Chart(cartBBICanvas);
      var dataBOS = <?php echo $cartBOS; ?>;
      var dataBOC = <?php echo $cartBOC; ?>;
      var dataINC = <?php echo $cartINC; ?>;

      var cartBBIData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ags", "Sep", "Oct", "Nov", "Des"],
        datasets: [
          {
            label: "BOS",
            fillColor: "rgb(210, 214, 222)",
            strokeColor: "rgb(210, 214, 222)",
            pointColor: "rgb(210, 214, 222)",
            pointStrokeColor: "#c1c7d1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgb(220,220,220)",
            data: dataBOS
          },
          {
            label: "BOC",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgba(60,141,188,0.8)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: dataBOC
          },
          {
            label: "Inconvinence",
            fillColor: "rgba(215, 44, 44, 0.7)",
            strokeColor: "rgba(215, 44, 44, 0.7)",
            pointColor: "#d72c2c",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: dataINC
          }
        ]
      };

      var cartBBIOptions = {
        showScale: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        bezierCurve: true,
        bezierCurveTension: 0.3,
        pointDot: false,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: false,
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
        maintainAspectRatio: true,
        responsive: true

      };

      cartBBI.Line(cartBBIData, cartBBIOptions);
    });
    </script>
    <script>
    $(function () {
      'use strict';
      var cartWKLCanvas = $("#cartWKL").get(0).getContext("2d");

      var cartWKL = new Chart(cartWKLCanvas);
      var dataWLF = <?php echo $cartWLF; ?>;
      var dataKIT = <?php echo $cartKIT; ?>;
      var dataLAS = <?php echo $cartLAS; ?>;

      var cartWKLData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ags", "Sep", "Oct", "Nov", "Des"],
        datasets: [
          {
            label: "Wild Life Hazard",
            fillColor: "rgb(210, 214, 222)",
            strokeColor: "rgb(210, 214, 222)",
            pointColor: "rgb(210, 214, 222)",
            pointStrokeColor: "#c1c7d1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgb(220,220,220)",
            data: dataWLF
          },
          {
            label: "Kites",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgba(60,141,188,0.8)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: dataKIT
          },
          {
            label: "Laser",
            fillColor: "rgba(215, 44, 44, 0.7)",
            strokeColor: "rgba(215, 44, 44, 0.7)",
            pointColor: "#d72c2c",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: dataLAS
          }
        ]
      };

      var cartWKLOptions = {
        showScale: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        bezierCurve: true,
        bezierCurveTension: 0.3,
        pointDot: false,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: false,
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
        maintainAspectRatio: true,
        responsive: true
      };

      cartWKL.Line(cartWKLData, cartWKLOptions);
    });
    </script>
    <script>
    $(function () {
      'use strict';
      var cartHGACanvas = $("#cartHGA").get(0).getContext("2d");

      var cartHGA = new Chart(cartHGACanvas);
      var dataHOL = <?php echo $cartHOL; ?>;
      var dataGOA = <?php echo $cartGOA; ?>;

      var cartHGAData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ags", "Sep", "Oct", "Nov", "Des"],
        datasets: [
          {
            label: "Holding",
            fillColor: "rgba(215, 44, 44, 0.7)",
            strokeColor: "rgba(215, 44, 44, 0.7)",
            pointColor: "#d72c2c",
            pointStrokeColor: "#d72c2c",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgb(220,220,220)",
            data: dataHOL
          },
          {
            label: "Go Around",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgba(60,141,188,0.8)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: dataGOA
          }
        ]
      };

      var cartHGAOptions = {
        showScale: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        bezierCurve: true,
        bezierCurveTension: 0.3,
        pointDot: false,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: false,
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
        maintainAspectRatio: true,
        responsive: true
      };

      cartHGA.Line(cartHGAData, cartHGAOptions);
    });
    </script>
    @endpush
@stop
