@extends('admin.layout.layout')
@section('content')
<div class="modal fade" id="modalAlert"  >
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Safety Alert</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form class="form-horizontal" id="addsafety" >

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Tanggal</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="datepicker" name="Tanggal" placeholder="pilih tanggal">
                                        <input type="hidden" class="form-control" name="Tahun" id="settahun" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Sumber</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="Sumber" class="form-control" id="Sumber" placeholder="Sumber" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Judul</label>

                                    <div class="col-sm-8">
                                        <input type="text" name="Judul" class="form-control" id="Judul" placeholder="Judul" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputnewPersonil" class="col-sm-2 control-label">Safety Alert</label>

                                    <div class="col-sm-8">
                                        <textarea type="text" name="Alert" class="form-control" rows="5" id="Alert" placeholder="Safety Alert" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                            <label for="inputnewPersonil" class="col-sm-2 control-label">attachment</label>
                                <div class="col-sm-8">
					                <div class="btn btn-default btn-file ">
					                  <i class="fa fa-paperclip"></i> Select Document
					                  <input type="file" name="attachment" id="attachment" onchange="setname()">
					                </div>
					                <label id="filelabel" class=" control-label"></label>
					                </div>
					            </div>                                                                

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button id="gsave" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> SAVE</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
     </div>
<section class="content-header">
      <h1>
        Safety Alert
      </h1>



    </section>


    <section class="content">
      <!-- row -->
      <div class="row">

        <div class="col-md-12">
        @if(Auth::user()->level)
        <a class="btn bg-olive btn-flat margin-bottom" id="tahunselect">
            <i class="fa fa-tags"></i> Filter Tahun
        </a>
        <a class="btn bg-olive btn-flat margin-bottom" id="tahunselect">
            <i class="fa fa-tags"></i> Filter Sumber
        </a>
        <a class="btn bg-olive btn-flat margin-bottom" data-toggle="modal" data-target="#modalAlert">
            <i class="fa fa-plus"></i> Add Safety Alert
        </a>
        @endif
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            @foreach ($months as $month => $items)

            <li class="time-label">
                  <span class="bg-yellow ceck" id="ceck">
                    {{ $month }}
                  </span>
            </li>
            @foreach ($items as $item)
	            <!-- /.timeline-label -->
            <!-- timeline item -->

            <li>
              <i class="fa fa-info-circle bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {!! date("d-m-Y",strtotime($item['Tanggal'])) !!}</span>

                <h3 class="timeline-header"><i class="fa fa-inbox"></i> <b>Sumber</b> : {!! $item['Sumber']!!} | <b>Tentang</b> : {!! $item['Judul']!!}</h3>

                <div class="timeline-body">
                 <p> {!! nl2br($item['Alert'])!!} </p>

                  @if( ! empty($item['Attach']))
                  <br>
                  <i class="fa fa-paperclip"></i> Attachments : <a target="_blank" href="{{ url('upload/safety/'.$item['Attach']) }}">{!! $item['Attach']!!}</a> 
                  @endif

                </div>
                <div class="timeline-footer">
                @if(Auth::user()->level)
                  <a class="btn btn-danger btn-xs"  id="hapusfile" data-id='{!! $item["id"] !!}'>Delete</a>
                @endif
                </div>
              </div>
            </li>

            @endforeach
            @endforeach
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- /.row -->

    </section>

    @push('style')
    <link href="{{asset('plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
     <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">
    @endpush
    @push('javascript')
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>
     <script src="{{asset('dist/js/date.js')}}"></script>
    <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/datepicker/bootstrap-datepicker.id.js')}}"></script>
    <script type="text/javascript">
    function setname(){
    	$( "#filelabel" ).text( $("#attachment").val().split('\\').pop()) ;
    }
    </script>
    <script type="text/javascript">
    	 $('#tahunselect').datepicker({
             viewMode: "years", 
    		minViewMode: "years",
            format: 'yyyy',
            autoclose: true,
            language: 'id',
        })
    	.on('changeDate', function(ev){
           window.location.href = " {{ url('safety/filter') }}/" + ev.format();
        });
    </script>
      <script>
      $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        })
    	.on('changeDate', function(ev){
           var dates = ev.format();
           var parts = dates.split("-");
           $('#settahun').val(parts[2]);
        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
   <script>
        function infoalert(title,data,tipe) {
            swal({
                title: title,
                text: data,
                type: tipe,
                timer: 2000,
                showConfirmButton: false
            });
        }
    </script>
    <script type="text/javascript">
    $(document).on("click",'.simpan',function (e) {
                e.preventDefault();
                var form = document.forms.namedItem("addsafety"); 
                var formdata = new FormData(form);
                 swal({
                      title: "Konfirmasi",
                      text: "Simpan Safety Alert",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                async: true,
                                type: "POST",
                                contentType: false,
                                dataType: "json",
                                url: "{{ url('http/post/safety') }}",
                                data: formdata, // high importance!
                                processData: false, // high importance!
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Data Sukses Di Input','success');
                                        location.reload();                                       
                                    }else{
                                        infoalert('Information','Nomer Sudah Digunakan','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });

    });
    </script>
       <script type="text/javascript">
    $(document).on("click",'#hapusfile',function (e) {
                e.preventDefault();
                var idfile = $(this).attr("data-id");
                 swal({
                      title: "Konfirmasi",
                      text: "Hapus Safety Alert",
                      type: "info",
                      showCancelButton: true,
                      closeOnConfirm: false,
                      showLoaderOnConfirm: true,
                    },
                    function(){
                        $.ajax({
                                type: "POST",
                                url: "{{ url('http/post/delsafety') }}",
                                data: {
                                    'namafile'      : idfile,
                                },
                                success: function (data) {
                                    if(data['sukses']==1){
                                        infoalert('Information','Safety Alert Sukses Dihapus','success');
                                        location.reload();                                       
                                    }else{
                                        infoalert('Information','Gagal Hapus Safety Alert','error');
                                    }

                                },
                                complete: function () {


                                }
                            });
                });

    });
    </script>
    <script type="text/javascript">
    	var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni","Juli", "Agustus", "September", "Oktober", "November", "Desember"];

	//var d = new Date();
	//alert("The current month is " + monthNames[d.getMonth()]);
	
    </script>
    @endpush
@stop
