@extends('admin.layout.layout')
@section('content')
    <div class="row">

        <div class="modal fade" id="modalAdduser"  >
            <div class="modal-dialog">
                <div class="modal-content">

                <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add New User</h3>
                </div>
                <!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal"  >

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputRole3" class="col-sm-2 control-label">Select Role</label>

                            <div class="col-sm-10">
                                <select name="level" class="form-control" id="inputRole3" >
                                    <option value="1">Administrator</option>
                                    <option value="0">Staff</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName3" class="col-sm-2 control-label">Nama</label>

                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="inputName3" placeholder="Nama" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                            <div class="col-sm-10">
                                <input type="text" name="password" class="form-control" id="inputPassword3" placeholder="Password" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button  id="gsave" class="btn btn-info pull-right">Register</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
         </div>
        </div>
        <div class="col-md-12">
            <a class="btn bg-olive btn-app" data-toggle="modal" data-target="#modalAdduser">
                <i class="fa fa-user-plus"></i> Add Users
            </a>
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List User</h3>
                </div>
                <div id="testdiv" class="box-body no-padding">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th style="width: 40px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1 ?>
                        @foreach($data as $lead)


                        <tr >
                            <td>{{$i++}}</td>
                            <td>{!! $lead['name'] !!}</td>

                            <td>
                                {!! $lead['email'] !!}
                            </td>
                            <td>
                                @if($lead['level'])
                                    Administrator
                                @else
                                    Staff
                                @endif

                            </td>
                            <td><button id="delete" data-id="{!! $lead['id'] !!}" class="btn btn-sm btn-danger delete">Delete</button> </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="box-footer clearfix">
                    {!! $data->links() !!}

                </div>
                </div>


        </div>

    </div>
    @push('style')
    <link href="{{asset('dist/css/jquery.toast.min.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/sweetalert.css')}}" rel="stylesheet">

    @endpush
    @push('javascript')
    <script>
        $(".pagination").addClass("pagination-sm no-margin pull-right");
        /*var $listItems = $('.pagination li');
        $('.pagination li a').click(function (e) {
            e.preventDefault();
            $("#testdiv").load($(this).attr('href')+' #testdiv');
            //$("#testdiv").hide().html(e).fadeIn('fast');
            //alert($(this).attr('href'));
        });
        */
    </script>
    <script src="{{asset('dist/js/sweetalert.min.js')}}"></script>

    <script>
            function randomPassword(length) {
                var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
                var pass = "";
                for (var x = 0; x < length; x++) {
                    var i = Math.floor(Math.random() * chars.length);
                    pass += chars.charAt(i);
                }
                return pass;
            }
            $('#inputPassword3').val(randomPassword(6));
        </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
           // var idpeserta = $(this).attr("data-id");
           function isEmail(email) {
               var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
               return regex.test(email);
           }
           function infoalert(title,data,tipe) {
               swal({
                   title: title,
                   text: data,
                   type: tipe,
                   timer: 2000,
                   showConfirmButton: false
               });
           }
            $('#gsave').on("click",function (e) {

                e.preventDefault();
                if(!isEmail($('#inputEmail3').val()))
                {
                    toast("Email Not Valid");
                }else {
                    if ($('#inputName3').val().trim() == '' || $('#inputemail3').val() == '' || $('#inputPassword3').val() == '') {
                        toast("Name and Email Can't Blank");
                    } else {
                        $('#gsave').attr('disabled', 'disabled');
                        $.ajax({
                            type: "POST",
                            url: "{{ url('http/auth/register') }}",
                            data: $('form.form-horizontal').serialize(),
                            success: function (data) {
                                //var res =JSON.stringify(data);
                                if (data['sukses'] == 1) {
                                    //sweetAlert("Success", "New User Has Been Created", "success");
                                    infoalert("Information!","New User Has Been Created","success")
                                    $("#modalAdduser").modal('hide');

                                    $("#testdiv").load("{!! url('')  !!}/adduser #testdiv");

                                } else {
                                    //toast("Email Already Registered");
                                    infoalert("Information!","Email Already Registered","error")
                                }
                            },
                            complete: function () {
                                // location.reload();
                                $("#gsave").removeAttr('disabled');
                                $('form.form-horizontal').trigger("reset");
                                $('#inputPassword3').val(randomPassword(6));

                            }
                        });
                    }
                }
            });
           $(document).on("click",'.delete',function (e) {
                e.preventDefault();
               var id = $(this).attr("data-id");
               swal({
                           title: "Are you sure?",
                           text: "",
                           type: "warning",
                           showCancelButton: true,
                           confirmButtonColor: "#DD6B55",
                           confirmButtonText: "Yes, delete it!",
                           cancelButtonText: "No, cancel!",
                           closeOnConfirm: false,
                           closeOnCancel: false
                       },
                       function(isConfirm){
                           if (isConfirm) {
                               $.ajax({
                                   type: "POST",
                                   url: "{{ url('http/auth/deleteuser') }}",
                                   data: {id : id},
                                   success: function (data) {
                                       //var res =JSON.stringify(data);
                                       if (data['sukses'] == 1) {
                                           infoalert("Information!","User Has Been Deleted","success")
                                           $("#testdiv").load("{!! url('')  !!}/adduser #testdiv");

                                       } else {
                                           toast("Can't Delete User");
                                       }
                                   },
                                   complete: function () {
                                       // location.reload();

                                   }
                               });

                           } else {
                               infoalert("Canceled!","","error")
                           }
                       });

                return false;
            });
    </script>
    @endpush
@stop
