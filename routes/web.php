 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'admin\dashboard@index')->middleware('auth');

Route::get('login',function(){
    return view('admin.login');
});

Route::post('http/auth/login', 'admin\apps@reqLogin');
Route::get('logout','admin\apps@reqLogout');


Route::group(['middleware' => ['sesi']],function () {

    Route::get('dashboard','admin\dashboard@index');
    //Router Action Plan
    Route::get('actionplan','admin\Plan@getData');
    Route::get('actionplan/view/{id}','admin\Plan@viewPlan');
    Route::get('actionplan/filter/{filter}/{id}','admin\Plan@filterPlan');
    Route::post('http/auth/updatemaction', 'admin\Plan@insertMitigation');

    //Route Hazard
    Route::get('hazard/AddHazard','admin\Hazard@getAddHazard');
    Route::get('hazard/ViewHazard','admin\Hazard@getViewHazard');
    Route::get('hazard/apptower','admin\Hazard@getIndex');
    Route::get('hazard/fss','admin\Hazard@getIndex');
    Route::get('hazard/ais','admin\Hazard@getIndex');
    Route::get('hazard/security','admin\Hazard@getIndex');
    Route::get('hazard/others','admin\Hazard@getIndex');

    //route profile
    Route::get('admin-setting/profile','admin\profile@index');
    Route::get('admin-setting/profile/data','admin\profile@data');
    Route::post('admin-setting/profile/update','admin\profile@update');
    Route::post('admin-setting/profile/photo','admin\profile@photo');


    //FilterHazard
    Route::get('hazard/printHazard/{tanggal}','admin\Hazard@printHazard');
    Route::get('hazard/apptower/{tanggal}','admin\Hazard@getApptower');
    Route::get('hazard/ViewHazard/{tanggal}','admin\Hazard@getViewFilteHazard');
    Route::get('hazard/fss/{tanggal}','admin\Hazard@getFss');
    Route::get('hazard/ais/{tanggal}','admin\Hazard@getAis');
    Route::get('hazard/security/{tanggal}','admin\Hazard@getSecurity');
    Route::get('hazard/others/{tanggal}','admin\Hazard@getOthers');
    Route::post('http/auth/insertHazard', 'admin\Hazard@insertHazard');


    //Dokumen
    Route::get('viewdokumen','admin\Dokumen@getIndex');
    Route::get('dokumen/tahun/{year}','admin\Dokumen@getIndexTahun');
    Route::get('dokumen/kategori/{cat}','admin\Dokumen@getIndexKategori');
    Route::get('dokumen/putdokumen','admin\Dokumen@getputDokumen');
    Route::post('http/post/postData', 'admin\Dokumen@insertData');
    Route::post('http/post/uploadData', 'admin\Dokumen@postUpload');
    Route::post('http/post/deletefile', 'admin\Dokumen@deleteFile');

    //Safety Alert
    Route::get('safety','admin\Alert@getIndex');
    Route::get('safety/filter/{tahun}','admin\Alert@getFilter');
    Route::post('http/post/safety', 'admin\Alert@postSafety');
    Route::post('http/post/delsafety', 'admin\Alert@deleteSA');

    //Manage Dasboard
    Route::get('managedashboard','admin\managedashboard@index');
    Route::get('managedashboard/insert','admin\managedashboard@addTopic');
    Route::get('managedashboard/{tanggal}','admin\managedashboard@getTanggal');
    Route::post('http/post/topic', 'admin\managedashboard@postTopic');
});

Route::group(['middleware' => ['admin']],function (){
    Route::get('adduser','admin\apps@reqUser');
    Route::post('http/auth/deleteuser', 'admin\apps@reqDelete');
    Route::post('http/auth/register', 'admin\apps@reqRegister');
    Route::post('http/auth/AddPlan', 'admin\Plan@AddPlan');
    Route::post('http/auth/insertData', 'admin\Plan@insertData');
    Route::post('http/auth/deletePlan', 'admin\Plan@deletePlan');
    Route::post('http/auth/UpdatePlan', 'admin\Plan@UpdatePlan');
    Route::post('http/auth/deleteoption', 'admin\Plan@DeleteOpion');
    Route::get('actionplan/editData/{id}','admin\Plan@updateData');
    Route::get('actionplan/insert', 'admin\Plan@InsertPlan');
});
