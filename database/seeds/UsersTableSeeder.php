<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$BDsL/LX.eKe04WtDfvwpaulLHB22mGDsh/T2PATuI1s1e8fIw1OnC',
                'level' => 1,
                'remember_token' => 'E6fvzF2KSP9hbZmIqYxc50FyPmaWYKjWsFMT0CPSGHRIs8BQScPlVoiny4gG',
                'created_at' => NULL,
                'updated_at' => '2016-12-13 11:22:28',
            ),
            1 => 
            array (
                'id' => 21,
                'name' => 'Staff Baru',
                'email' => 'admin@admin.comaa',
                'password' => '$2y$10$3AAvkqkQz56LbkoyJ7vGO..z.iDD48ijcu6lTDuhVUe2.7mEtWa9G',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2016-11-23 10:28:55',
                'updated_at' => '2016-11-23 10:28:55',
            ),
            2 => 
            array (
                'id' => 22,
                'name' => 'Staff Baru',
                'email' => 'staaff@admin.coma',
                'password' => '$2y$10$xNbBrNiePE7JcUb0YtWoE.OjJImuxKglHqVlGVUDzszvMjSxh87za',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2016-11-23 10:29:11',
                'updated_at' => '2016-11-23 10:29:11',
            ),
            3 => 
            array (
                'id' => 24,
                'name' => 'Staff Baru',
                'email' => 'guest@guest.com',
                'password' => '$2y$10$cFW.SAG7wyVk9WZVHkMrgelrZ.hTqV7BAm5wyPC2X7hR69BKv5SEm',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2016-11-25 09:34:48',
                'updated_at' => '2016-11-25 09:34:48',
            ),
            4 => 
            array (
                'id' => 25,
                'name' => 'admin',
                'email' => 'staff@admin.comsssssssss',
                'password' => '$2y$10$m43oo90GfhJQ7o77j0zAO.5hr4wmZ7hiMBsTNNZY3m0bFxSDWKON2',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2016-12-18 02:14:02',
                'updated_at' => '2016-12-18 02:14:02',
            ),
            5 => 
            array (
                'id' => 26,
                'name' => 'asdasd',
                'email' => 'admin@adasdasdasdmin.com',
                'password' => '$2y$10$0ynr.26reqeuHbqUQy7ZqObhXOWrPTG9RXoqd/kjx5suHxGHHrCgW',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2016-12-18 03:28:45',
                'updated_at' => '2016-12-18 03:28:45',
            ),
            6 => 
            array (
                'id' => 28,
                'name' => 'qwdqwdq',
                'email' => 'qwdq@asdad.com',
                'password' => '$2y$10$Wr052QivYgQ5xfdOW5CzmOiPYtyWLeVs61rly2LhCffwliIH99iyi',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2016-12-19 11:32:09',
                'updated_at' => '2016-12-19 11:32:09',
            ),
        ));
        
        
    }
}
