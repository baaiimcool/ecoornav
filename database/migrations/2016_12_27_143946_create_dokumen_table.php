<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDokumenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dokumen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('Nama');
			$table->decimal('Tahun', 10, 0);
			$table->string('Kategori');
			$table->date('Publikasi');
			$table->string('Filedokumen');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dokumen');
	}

}
