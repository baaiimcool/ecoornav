<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionplanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actionplan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('Nomer')->unique('Nomer');
			$table->date('Tanggal');
			$table->string('Unit')->nullable();
			$table->string('Personil')->nullable();
			$table->text('Description', 65535)->nullable();
			$table->string('SRI1')->nullable();
			$table->text('Finding', 65535)->nullable();
			$table->string('Recomendation')->nullable();
			$table->string('PIC')->nullable();
			$table->string('Timeline')->nullable();
			$table->text('M_Action', 65535)->nullable();
			$table->string('C_Status')->nullable();
			$table->string('SRI2')->nullable();
			$table->boolean('isUpdate');
			$table->string('UpdateBy');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actionplan');
	}

}
