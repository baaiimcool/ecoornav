<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->date('Tanggal')->unique();
            $table->integer('BOS');
            $table->integer('BOC');
            $table->integer('Inconvinience');
            $table->integer('WildLife');
            $table->integer('Kites');
            $table->integer('Laser');
            $table->integer('Holding');
            $table->integer('GoAround');
            $table->primary('Tanggal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
