<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHazardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hazard', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('Operasi', 25);
			$table->text('HazardUmum', 65535)->nullable();
			$table->text('KomponenHazard', 65535)->nullable();
			$table->text('KonsekuensiHazard', 65535)->nullable();
			$table->text('ResikoDilakukan', 65535)->nullable();
			$table->string('SRI')->nullable();
			$table->text('TindakLanjut', 65535)->nullable();
			$table->text('Kondisi', 65535)->nullable();
			$table->string('CRI')->nullable();
			$table->date('tanggal');
			$table->string('Tipe');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hazard');
	}

}
