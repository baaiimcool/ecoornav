<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSafetyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('safety', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('Judul');
			$table->string('Sumber');
			$table->date('Tanggal');
			$table->text('Alert', 65535);
			$table->integer('Tahun');
			$table->string('Attach')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('safety');
	}

}
