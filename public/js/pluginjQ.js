/**
 * Created by rachman on 28/10/2016.
 */
$.fn.appSimpleSubmit = function (callback,before) {
    var self = $(this);
    self.submit(function (e) {
        e.preventDefault();
        typeof before == "function" && before();
        $.ajax({
            url:self.attr('action'),
            type:self.attr('method').toUpperCase(),
            dataType :typeof self.attr('app-data-type') != 'undefined' ? self.attr('app-data-type'):'json',
            data:self.serializeArray(),
            success: function(result,status,xhr){
                return typeof callback != "undefined" ? callback(result,status,xhr) : '';
            },
            error: function(xhr,status,error){
                return typeof callback != "undefined" ? callback(error,status,xhr) : '';
            }
        });

    });
};
$.fn.onSimpleSubmit = function (callback,before) {
    $('body').on('submit',$(this).selector,function (e) {
        e.preventDefault();
        var self = $(this);
        typeof before == "function" && before();
        $.ajax({
            url:self.attr('action'),
            type:self.attr('method').toUpperCase(),
            dataType :typeof self.attr('app-data-type') != 'undefined' ? self.attr('app-data-type'):'json',
            data:self.serializeArray(),
            success: function(result,status,xhr){
                return typeof callback != "undefined" ? callback(result,status,self,xhr) : '';
            },
            error: function(xhr,status,error){
                return typeof callback != "undefined" ? callback(error,status,self,xhr) : '';
            }
        });
    });
};
$.fn.appAjax = function (object, callback) {
    if(typeof object == "object"){
        var self = $( this );
        $.ajax({
            url:self.attr('app-data-url'),
            type:self.attr('app-data-method').toUpperCase(),
            dataType :typeof self.attr('app-data-type') != 'undefined' ? self.attr('app-data-type'):'json',
            data:object,
            success: function(result,status,xhr){
                callback(result,status,xhr);
            },
            error: function(xhr,status,error){
                callback(error,status,xhr);
            }
        });
    }
};
$.fn.ajaxOnLoad = function (callback) {
    var self = $( this );
    $.ajax({
        url:self.attr('app-data-url'),
        type:self.attr('app-data-method').toUpperCase(),
        dataType :typeof self.attr('app-data-type') != 'undefined' ? self.attr('app-data-type'):'json',
        success: function(result,status,xhr){
            callback(result,status,xhr);
        },
        error: function(xhr,status,error){
            callback(error,status,xhr);
        }
    });
};
//just add