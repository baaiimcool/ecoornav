var swicher = function(swich){
    var init = {
        id   : '.sw-id',
        show : '.sw-show',
        link : 'a[sw-init]'
    };
    this.getInit= function(){
        return init;
    };
    typeof swich != "undefined" && $.extend(init,swich);
};
swicher.prototype.exe = function (sw_data) {
    var id = this.getInit().id, show = this.getInit().show,link=this.getInit().link;
    $(id).hide();
    $(show).show();
    $('body').on('click',link,function (e) {
        e.preventDefault();
        var target = $(this).attr('href').replace(/\s/g,'');
        if(typeof sw_data != "undefined"){
            sw_data(function () {
                $(id).fadeOut(200);
                $(target+id).fadeIn(300);
            },function (reff) {
                var result = false;
                if(reff.replace(/\s/g,'') === target){
                    result = true;
                }
                return result;
            },target);
        }else{
            $(id).fadeOut(200);
            $(target+id).fadeIn(300);
        }
    });
};