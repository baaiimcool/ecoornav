-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2016 at 10:51 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bandara`
--

-- --------------------------------------------------------

--
-- Table structure for table `hazard`
--

CREATE TABLE `hazard` (
  `id` int(11) NOT NULL,
  `Operasi` varchar(25) NOT NULL,
  `HazardUmum` text,
  `KomponenHazard` text,
  `KonsekuensiHazard` text,
  `ResikoDilakukan` text,
  `SRI` varchar(255) DEFAULT NULL,
  `TindakLanjut` text,
  `Kondisi` text,
  `CRI` varchar(255) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `Tipe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hazard`
--

INSERT INTO `hazard` (`id`, `Operasi`, `HazardUmum`, `KomponenHazard`, `KonsekuensiHazard`, `ResikoDilakukan`, `SRI`, `TindakLanjut`, `Kondisi`, `CRI`, `tanggal`, `Tipe`) VALUES
(1, 'FASILITAS', 'MANTAP COEG', 'qwdqwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'ntap Soul', 'qwdqwd', '2016-12-01', 'APPTOWER'),
(2, 'SDM', 'MANTAP NJENK', 'qwdqwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'ntap Soul', 'qwdqwd', '2016-12-05', 'FSS'),
(3, 'SDM', 'DOble MANTAP NJENK', 'qwdqwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'ntap Soul', 'qwdqwd', '2016-12-01', 'OTHERS'),
(4, 'PROCEDURE', 'DOble MANTAP NJENK', 'qwdqwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'qwdqwd', 'ntap Soul', 'qwdqwd', '2016-12-06', 'APPTOWER'),
(5, 'FASILITAS', '<font color=''blue''>qwdqwdqwdq</font>', 'qwd', 'qwdqwd', 'qwd', NULL, 'qwd', 'qwdqwd', 'qwd', '2016-12-02', 'APPTOWER'),
(6, 'FASILITAS', '<font color=''blue''>qwdqwd</font>', 'qwdqwd', 'qwdqwd', 'qwdqwd', NULL, 'qwdqwdqwd', '', 'qwdqwd', '2016-12-27', 'APPTOWER'),
(7, 'FASILITAS', 'asdasdasd', 'asdasdasd', '', '', NULL, '', '', '', '2016-12-04', 'APPTOWER'),
(8, 'FASILITAS', '<font color=''blue''>qwdqwd</font>', 'qwdqwd', '', '', NULL, '', '', '', '2016-12-19', 'APPTOWER'),
(9, 'FASILITAS', 'qwdqwdqwd', '', '', '', NULL, '', 'qwdqwd', 'qwdqw', '2016-12-02', 'APPTOWER'),
(10, 'PROCEDURE', '<font color=''blue''>qwdqwdqwd</font>', '', '', '', NULL, '', '', 'qwdqwd', '2016-12-22', 'APPTOWER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hazard`
--
ALTER TABLE `hazard`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hazard`
--
ALTER TABLE `hazard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
