<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Model\Actionplan;
use App\Model\Personil;
use App\Model\Unit;
use App\Model\SRI;
use View;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class Plan extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }
    public function viewPlan($id)
    {
        $data = Actionplan::where('id', $id)->paginate(2);
        $personils = Personil::All();
        $unit = Unit::All();
        $sri = SRI::All();
        return View::make("admin.actionplan",compact('data','personils','unit','sri'));
    }
    public function filterPlan($filter,$id)
    {
        if($filter=='byname'){
            $data = Actionplan::where('UpdateBy', $id)->get();
        }
        elseif ($filter=='isEdit') {
            $data = Actionplan::where('isUpdate', $id)->get();
        }
        $personils = Personil::All();
        $unit = Unit::All();
        $sri = SRI::All();
        return View::make("admin.actionplan",compact('data','personils','unit','sri'));
    }
    public function getData()
    {
        $data = Actionplan::orderBy('Nomer', 'asc')->paginate(2);
        $personils = Personil::All();
        $unit = Unit::All();
        $sri = SRI::All();
        return View::make("admin.actionplan",compact('data','personils','unit','sri'));
    }
    public function updateData($id)
    {
        $data = Actionplan::where('id', $id)->get();
        $personils = Personil::All();
        $unit = Unit::All();
        $sri = SRI::All();
        return View::make("admin.insertplan",compact('data','personils','unit','sri'));
    }
     protected function UpdatePlan(Request $request){
        try{
        $getdata = $request->input();
        $idaction = $request->Nomer;
        $data = array_filter($getdata,'strlen');
        Actionplan::where('Nomer', $idaction)
                    ->update($data);
         return response()->json([
                        'sukses' => 1,
                    ]);
         }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => 0,
                    ]);
                }
    }
    protected function insertMitigation(Request $request){
        try{
        $idaction = $request->idaction;
        $dataaction = $request->dataaction;
        $data = array('M_Action' => $dataaction,
                                    'isUpdate'=>true,
                                    'UpdateBy'=> Auth::user()->name);
        Actionplan::where('id', $idaction)
                    ->update($data);
         return response()->json([
                        'sukses' => '1',
                    ]);
         }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
    }
    protected function deletePlan(Request $request){
        try{
        $idplan = $request->idplan;
        Actionplan::where('id', $idplan)->delete();
         return response()->json([
                        'sukses' => '1',
                    ]);
         }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
    }
    public function InsertPlan(){
        $personils = Personil::All();
        $unit = Unit::All();
        $sri = SRI::All();
        return View::make("admin.insertplan",compact('personils','unit','sri'));
    }
    protected function insertData(Request $request){
        if($request->fungsi == 'personil')
        {
            try{
            $personils = new Personil;
            $personils->Personil =  $request->data;
            $personils->save();
                    return response()->json([
                    'sukses' => '1',
                    ]);
                }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
        }
        elseif ($request->fungsi == 'unit') {
            try{
            $unit = new Unit;
            $unit->Unit =  $request->data;
            $unit->save();
                    return response()->json([
                    'sukses' => '1',
                    ]);
                }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
        }
        elseif ($request->fungsi == 'sri') {
            try{
                $sri = new SRI;
                $sri->SRI =  $request->data;
                $sri->color =  $request->color;
                $sri->save();
                    return response()->json([
                    'sukses' => '1',
                    ]);
                }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }

        }

    }
    public function AddPlan(Request $request)
    {
        $data = new Actionplan;
        $data->Nomer = $request->Nomer;
        $data->Tanggal = date("Y-m-d",strtotime($request->input('Tanggal')));
        $data->Unit = $request->Unit;
        $data->Personil = $request->Personil;
        $data->Description = $request->Description;
        $data->SRI1 = $request->SRI1;
        $data->Finding = $request->Finding;
        $data->Recomendation = $request->Recomendation;
        $data->PIC = $request->PIC;
        $data->Timeline = $request->Timeline;
        $data->M_Action = $request->M_Action;
        $data->C_Status = $request->C_Status;
        $data->SRI2 = $request->SRI2;
        $data->isUpdate = false;
        $data->UpdateBy = Auth::user()->name;
        try{
            $data->save();
            return response()->json([
                'sukses' => '1',
            ]);
        }catch(\Illuminate\Database\QueryException $e)
        {
           return response()->json([
                'sukses' => '0',
            ]);
        }

    }

    protected function DeleteOpion(Request $request){
        try{
        $idDelete = $request->methoddelete;
        $datadelete = $request->datadelete;
        if($idDelete == 'inputSRI')
        {
            SRI::where('SRI', $datadelete)->delete();
             return response()->json([
                            'sukses' => '1',
                        ]);
        }
        elseif ($idDelete == 'inputUnit') {
            # code...
            Unit::where('Unit', $datadelete)->delete();
             return response()->json([
                            'sukses' => '1',
                        ]);
        }
        elseif ($idDelete == 'inputPersonil') {
            # code...
            Personil::where('Personil', $datadelete)->delete();
             return response()->json([
                            'sukses' => '1',
                        ]);
        }
         }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
    }
}
