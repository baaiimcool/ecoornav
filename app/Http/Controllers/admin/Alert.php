<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Model\Safety;
use Carbon\Carbon;
use View;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;



class Alert extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }
    public function getIndex()
    {

        $data = Safety::where('Tahun', date("Y"))->orderBy('Tanggal','DESC')->get();

        $months = [];   

        foreach ($data as $item) {
            $date = new Carbon($item['Tanggal']);
            $months[$date->format("F Y")][] = $item;
        }

        return View::make("admin.safetyalert",compact('months'));
    }
    public function getFilter($tahun)
    {

        $data = Safety::where('Tahun', $tahun)->orderBy('Tanggal','DESC')->get();
        
        $months = [];

        foreach ($data as $item) {
            $date = new Carbon($item['Tanggal']);
            $months[$date->format("F Y")][] = $item;
        }

        return View::make("admin.safetyalert",compact('months'));
    }
     protected function postSafety() {
        try{
            $file = Input::file('attachment');
            $destinationPath = 'upload/safety/';
            $Safety = new Safety;
                $Safety->Judul =  Input::input('Judul');
                $Safety->Sumber =  Input::input('Sumber');
                $Safety->Tanggal =  date("Y-m-d",strtotime(Input::input('Tanggal')));
                $Safety->Alert =  Input::input('Alert');
                $Safety->Tahun =  Input::input('Tahun');
                
                
            if (!Input::hasFile('attachment'))
            {
                $Safety->Attach =  "";
                $Safety->save(); 
                return response()->json([
                    'sukses' => '1',
                ]);
            }else{

                $ext = Input::input('Tanggal')."_".Input::file('attachment')->getClientOriginalName();
                //$fileName = time() .'_'. str_random(10).'_'. str_random(10).".". $ext;
                if(Input::file('attachment')->move($destinationPath, $ext))
                {
                    $Safety->Attach =  $ext;
                    $Safety->save(); 
                            return response()->json([
                            'sukses' => '1',
                            ]);
                    
                }else
                {   
                    return response()->json([
                            'sukses' => '0',
                        ]);
                }
            }
            }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => $e->getMessage(),
                    ]);
            }
    
    }
    protected function deleteSA(Request $request){
        try{
        $idfile = $request->namafile;
        $data = Safety::where('id', $idfile)->pluck('Attach');

        if ($data[0]=="")
        {
            Safety::where('id', $idfile)->delete();
             return response()->json([
                        'sukses' => '1',
                    ]);  
        }else
        {
            if(File::delete('upload/safety/'.$data[0]))
            {
            Safety::where('id', $idfile)->delete();
             return response()->json([
                        'sukses' => '1',
                    ]);  
            }
            }
        } 
        catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => $e,
                    ]);    
                } 
    }
}