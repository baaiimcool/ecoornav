<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Carbon\Carbon;
use fx3costa\laravelchartjs;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Model\Topic;
use View;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class dashboard extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }

    public function index(){
        $tanggalnow = Carbon::now();
        $tanggal = $tanggalnow->format('l, j F Y');
        $tanggals = $tanggalnow->toDateString();
        $Topic = Topic::where('Tanggal', $tanggals)->orderBy('Tanggal','DESC')->get();

        $cart = Topic::select(DB::raw("SUM(BOS) AS BOS , SUM(BOC) AS BOC , SUM(Inconvinience) AS Inconvinience, SUM(WildLife) AS WildLife, SUM(Kites) AS Kites, SUM(Laser) AS Laser, SUM(Holding) AS Holding, SUM(GoAround) AS GoAround"))
                    ->whereYear('Tanggal', '=', date("Y"))
                    ->groupBy(DB::raw("MONTH(Tanggal)"))
                    ->get()->toArray();

        $cartBOS = array_column($cart, 'BOS');
        $cartBOC = array_column($cart, 'BOC');
        $cartINC = array_column($cart, 'Inconvinience');
        $cartWLF = array_column($cart, 'WildLife');
        $cartKIT = array_column($cart, 'Kites');
        $cartLAS = array_column($cart, 'Laser');
        $cartHOL = array_column($cart, 'Holding');
        $cartGOA = array_column($cart, 'GoAround');

        return view('admin.dashboard')
        ->with('cartBOS',json_encode($cartBOS,JSON_NUMERIC_CHECK))
        ->with('cartBOC',json_encode($cartBOC,JSON_NUMERIC_CHECK))
        ->with('cartINC',json_encode($cartINC,JSON_NUMERIC_CHECK))
        ->with('cartWLF',json_encode($cartWLF,JSON_NUMERIC_CHECK))
        ->with('cartKIT',json_encode($cartKIT,JSON_NUMERIC_CHECK))
        ->with('cartLAS',json_encode($cartLAS,JSON_NUMERIC_CHECK))
        ->with('cartHOL',json_encode($cartHOL,JSON_NUMERIC_CHECK))
        ->with('cartGOA',json_encode($cartGOA,JSON_NUMERIC_CHECK))
        ->with('Topic', $Topic);
    }


}
