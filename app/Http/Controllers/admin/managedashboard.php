<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Model\Topic;
use View;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class managedashboard extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }

    public function index(){
    	$tanggalnow = Carbon::now();
    	$tanggal = $tanggalnow->format('l, j F Y');
    	$tanggals = $tanggalnow->toDateString();
    	$Topic = Topic::where('Tanggal', $tanggals)->orderBy('Tanggal','DESC')->get();

        return View::make("admin.mandashboard",compact('tanggal','Topic'));
    }

    public function getTanggal($tanggal){

    	$Topic = Topic::where('Tanggal', $tanggal)->orderBy('Tanggal','DESC')->get();

        return View::make("admin.mandashboard",compact('tanggal','Topic'));
    }

    public function addTopic(){
    	return view('admin.addTopic');
    }

    public function postTopic(Request $request)
    {
        $data = new Topic;
        $data->Tanggal = date("Y-m-d",strtotime($request->input('Tanggal')));
        $data->BOS = $request->BOS;
        $data->BOC = $request->BOC;
        $data->Inconvinience = $request->Inconvinience;
        $data->WildLife = $request->WildLife;
        $data->Kites = $request->Kites;
        $data->Laser = $request->Laser;
        $data->Holding = $request->Holding;
        $data->GoAround = $request->GoAround;
        try{
            $data->save();    
            return response()->json([
                'sukses' => '1',
            ]);
        }catch(\Illuminate\Database\QueryException $e)
        {
           return response()->json([
                'sukses' => '0',
            ]);    
        }  
        
    }

}
