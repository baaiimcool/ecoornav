<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Model\Actionplan;
use App\Model\Personil;
use App\Model\Unit;
use App\Model\SRI;
use App\Model\HazardTable;
use View;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class Hazard extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }
    public function getIndex()
    {
         return view('admin.Hazard.appHazard');
    }
    public function getViewHazard()
    {
        return view('admin.Hazard.appHazard');
    }
    public function printHazard($tanggal)
    {
        $no = 1;
        $data = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'')->get();

        $pdf = \PDF::loadView('admin.Hazard.printHazard',compact('no','facility','prosedur','sdm','data'));
        return $pdf->setPaper('a4', 'landscape')->setWarnings(false)->download("OutputHazard$tanggal.pdf");
    }
    public function getViewFilteHazard($tanggal)
    {
          $no = 1;
            $data = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'')->get();
            return View::make('admin.Hazard.viewHazard',compact('no','facility','prosedur','sdm','data'));
    }
    public function getApptower($tanggal)
    {
        $no = 1;
        //$filterfas = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'FASILITAS','Tipe' => 'APPTOWER'];
        //$filterpro= ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'PROCEDURE','Tipe' => 'APPTOWER'];
       // $filtersdm = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'SDM','Tipe' => 'APPTOWER'];

        //$facility = HazardTable::where( DB::raw('MONTH(tanggal)', '=', '12' ))->get();
        $facility = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="FASILITAS" AND Tipe="APPTOWER" AND ')->get();
        $prosedur = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="PROCEDURE" AND Tipe="APPTOWER" AND ')->get();
        $sdm = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="SDM" AND Tipe="APPTOWER" AND ')->get();

        //$facility = HazardTable::where($filterfas)->get();
        //echo date("Y-M",strtotime($tanggal));
        //$prosedur = HazardTable::where($filterpro)->get();
        //$sdm = HazardTable::where($filtersdm)->get();
         return View::make('admin.Hazard.appHazard',compact('no','facility','prosedur','sdm'));
    }
     public function getFss($tanggal)
    {
        $no = 1;/*
        $filterfas = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'FASILITAS','Tipe' => 'FSS'];
        $filterpro= ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'PROCEDURE','Tipe' => 'FSS'];
        $filtersdm = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'SDM','Tipe' => 'FSS'];

        $facility = HazardTable::where($filterfas)->get();
        $prosedur = HazardTable::where($filterpro)->get();
        $sdm = HazardTable::where($filtersdm)->get();
        */
         $facility = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="FASILITAS" AND Tipe="FSS" AND ')->get();
        $prosedur = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="PROCEDURE" AND Tipe="FSS" AND ')->get();
        $sdm = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="SDM" AND Tipe="FSS" AND ')->get();
         return View::make('admin.Hazard.appHazard',compact('no','facility','prosedur','sdm'));
    }
    public function getAis($tanggal)
    {
        $no = 1;
        /*
        $filterfas = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'FASILITAS','Tipe' => 'AIS'];
        $filterpro= ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'PROCEDURE','Tipe' => 'AIS'];
        $filtersdm = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'SDM','Tipe' => 'AIS'];

        $facility = HazardTable::where($filterfas)->get();
        $prosedur = HazardTable::where($filterpro)->get();
        $sdm = HazardTable::where($filtersdm)->get();
        */
        $facility = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="FASILITAS" AND Tipe="AIS" AND ')->get();
        $prosedur = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="PROCEDURE" AND Tipe="AIS" AND ')->get();
        $sdm = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="SDM" AND Tipe="AIS" AND ')->get();
         return View::make('admin.Hazard.appHazard',compact('no','facility','prosedur','sdm'));
    }
    public function getSecurity($tanggal)
    {
        $no = 1;
        /*
        $filterfas = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'FASILITAS','Tipe' => 'SECURITY'];
        $filterpro= ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'PROCEDURE','Tipe' => 'SECURITY'];
        $filtersdm = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'SDM','Tipe' => 'SECURITY'];

        $facility = HazardTable::where($filterfas)->get();
        $prosedur = HazardTable::where($filterpro)->get();
        $sdm = HazardTable::where($filtersdm)->get();
        */
        $facility = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="FASILITAS" AND Tipe="SECURITY" AND ')->get();
        $prosedur = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="PROCEDURE" AND Tipe="SECURITY" AND ')->get();
        $sdm = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="SDM" AND Tipe="SECURITY" AND ')->get();

         return View::make('admin.Hazard.appHazard',compact('no','facility','prosedur','sdm'));
    }
    public function getOthers($tanggal)
    {
        $no = 1;
        /*
        $filterfas = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'FASILITAS','Tipe' => 'OTHERS'];
        $filterpro= ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'PROCEDURE','Tipe' => 'OTHERS'];
        $filtersdm = ['tanggal' => date("Y-m-d",strtotime($tanggal)),'Operasi' => 'SDM','Tipe' => 'OTHERS'];

        $facility = HazardTable::where($filterfas)->get();
        $prosedur = HazardTable::where($filterpro)->get();
        $sdm = HazardTable::where($filtersdm)->get();
        */
        $facility = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="FASILITAS" AND Tipe="OTHERS" AND ')->get();
        $prosedur = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="PROCEDURE" AND Tipe="OTHERS" AND ')->get();
        $sdm = HazardTable::where( DB::raw('MONTH(tanggal)'), '=', $tanggal,'AND Operasi="SDM" AND Tipe="OTHERS" AND ')->get();

         return View::make('admin.Hazard.appHazard',compact('no','facility','prosedur','sdm'));
    }

    public function getAddHazard()
    {
        $sri = SRI::All();
        return View::make("admin.Hazard.AddHazard",compact('sri'));
    }
    public function insertHazard(Request $request)
    {
        $data = new HazardTable;
        $data->Operasi = $request->Operasi;
        if ($request->color == "blue") {
            $data->HazardUmum = "<font color='blue'>$request->Humum</font>";
        }else
        {
            $data->HazardUmum = $request->Humum;
        }
        $data->KomponenHazard = $request->SKHazard;
        $data->KonsekuensiHazard = $request->KHazard;
        $data->ResikoDilakukan = $request->Resiko;
        $data->SRI = $request->SRI;
        $data->TindakLanjut = $request->TindakLanjut;
        $data->Kondisi = $request->KondisiT;
        $data->CRI = $request->CRI;
        $data->tanggal = date("Y-m-d",strtotime($request->input('Tanggal')));
        $data->Tipe = $request->Tipe;
        try{
            $data->save();
            return response()->json([
                'sukses' => '1',
            ]);
        }catch(\Illuminate\Database\QueryException $e)
        {
           return response()->json([
                'sukses' => '0',
            ]);
        }
    }

}
