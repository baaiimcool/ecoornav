<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use View;

class apps extends Controller
{
    use RegistersUsers;

    //Login Controller
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }
    protected function reqLogin(Request $request){
        $username = $request->input('email');
        $password = $request->input('password');
        if (Auth::attempt(['email' => $username, 'password' => $password],false)) {
            return redirect('dashboard');
        }
        else{
            return redirect('login');
        }

    }
    public function reqLogout(){
        Auth::logout();
        return redirect('login');

    }
    public function reqUser(){
    //    Auth::logout();
        $data= User::paginate(8);
        return view("admin.addUser")->with('data',$data);

    }
    protected function reqDelete(Request $request)
    {
        $id = $request->input('id');
        try {
            $user = User::find($id);
            if($user->delete()) {
                return response()->json([
                    'sukses' => '1',
                ]);
            }
        }catch (\Illuminate\Database\QueryException $e){
            return response()->json([
                'sukses' => '0',
            ]);
        }
    }
    protected function reqRegister(Request $request)
    {
        $username = $request->input('email');
        $password = $request->input('password');
        $level = $request->input('level');
        $nama = $request->input('name');

        try {
            User::create([
                'name' => $nama,
                'email' => $username,
                'password' => bcrypt($password),
                'level' => $level,

            ]);
            return response()->json([
                'sukses' => '1',
            ]);
        }catch (\Illuminate\Database\QueryException $e){
            return response()->json([
                'sukses' => '0',
            ]);
        }

    }
}
