<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class profile extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);
    }
    public function index(){
        return view('admin.profile');
    }
    public function data(){
        $user=User::where('id',Auth::user()->id)->first();
        return response()->json($user);
    }
    public function update(Request $request){
        $req['name'] =$request->input('name');
        $req['email']=$request->input('email');
        if($request->has('newpass')) $req['password']=Hash::make($request->input('newpass'));
        User::where('id',Auth::user()->id)->update($req);
        $user=User::where('id',Auth::user()->id)->first();
        return response()->json($user);
    }
    public function photo(Request $request){
        $img = Image::make($request->file('uploadImg'));
        $img->fit(200, 300);
        $name=Auth::user()->id;
        $img->save('upload/images/imgprofile_'.$name.'.jpg');
    }
}
