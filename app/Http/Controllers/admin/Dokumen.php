<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Model\Dkategori;
use App\Model\Dtahun;
use App\Model\Document;
use View;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class Dokumen extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['reqLogin','reqLogout']]);

    }
    public function getIndex()
    {
        $tahun = Dtahun::orderBy('Tahun','DESC')->get();
        $kategori = Dkategori::All();
        $Document = Document::paginate(50);
        return View::make("admin.Dokumen.dokumen",compact('tahun','kategori','Document'));
    }
    public function getIndexTahun($year)
    {
        $tahun = Dtahun::orderBy('Tahun','DESC')->get();
        $kategori = Dkategori::All();
        $Document = Document::where('Tahun', $year)->paginate(50);
        return View::make("admin.Dokumen.dokumen",compact('tahun','kategori','Document'));
    }
    public function getIndexKategori($cat)
    {
        $tahun = Dtahun::orderBy('Tahun','DESC')->get();
        $kategori = Dkategori::All();
        $Document = Document::where('Kategori', $cat)->paginate(50);
        return View::make("admin.Dokumen.dokumen",compact('tahun','kategori','Document'));
    }
    public function getputDokumen()
    {
        $tahun = Dtahun::orderBy('Tahun','DESC')->get();
        $kategori = Dkategori::All();
        return View::make("admin.Dokumen.tambahdokumen",compact('tahun','kategori'));
    }
    protected function insertData(Request $request){
        if($request->fungsi == 'Tahun')
        {
            try{
            $Tahun = new Dtahun;
            $Tahun->Tahun =  $request->data;
            $Tahun->save();
                    return response()->json([
                    'sukses' => '1',
                    ]);
                }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
        }
        elseif ($request->fungsi == 'Kategori') {
            try{
            $Kategori = new Dkategori;
            $Kategori->Kategori =  $request->data;
            $Kategori->save();
                    return response()->json([
                    'sukses' => '1',
                    ]);
                }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => '0',
                    ]);
                }
        }

    }
    protected function postUpload() {
        try{
            $file = Input::file('attachment');
            $destinationPath = 'upload/dokumen/';
            $ext = Input::file('attachment')->extension();
            $fileName = str_replace(' ', '_', Input::input('nama')."_".Input::input('tanggal')).".". $ext;
            if(Input::file('attachment')->move($destinationPath, $fileName ))
            {
                $Document = new Document;
                $Document->Nama =  Input::input('nama');
                $Document->Tahun =  Input::input('tahun');
                $Document->Kategori =  Input::input('kategori');
                $Document->Publikasi =  date("Y-m-d",strtotime(Input::input('tanggal')));
                $Document->Filedokumen =  $fileName;
                $Document->save();
                        return response()->json([
                        'sukses' => '1',
                        ]);

            }else
            {
                return response()->json([
                        'sukses' => '0',
                    ]);
            }
            }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => Input::input('tanggal'),
                    ]);
            }

    }
    protected function deleteFile(Request $request){
        try{
        $idfile = $request->namafile;
        if (!File::exists('upload/dokumen/'.$idfile))
        {
         Document::where('Filedokumen', $idfile)->delete();
         return response()->json([
                        'sukses' => '1',
                    ]);
        }
        if(File::delete('upload/dokumen/'.$idfile)){
       Document::where('Filedokumen', $idfile)->delete();
         return response()->json([
                        'sukses' => '1',
                    ]);
        }else{
            return response()->json([
                        'sukses' => 'tak ada file',
                    ]);
        }
         }catch(\Illuminate\Database\QueryException $e)
                {
                   return response()->json([
                        'sukses' => $e,
                    ]);
                }
    }
}
