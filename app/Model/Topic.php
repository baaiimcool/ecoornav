<?php

namespace App\model;
use Eloquent; // ******** This Line *********
use Illuminate\Database\Eloquent\Model;

class Topic extends Model 
{
    //
    protected $table = 'topics';
    public $timestamps = false;

}
