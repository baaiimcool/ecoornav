<?php

namespace App\model;
use Eloquent; // ******** This Line *********
use Illuminate\Database\Eloquent\Model;

class Unit extends Model 
{
    //
    protected $table = 'unit';
    public $timestamps = false;

}
