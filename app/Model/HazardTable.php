<?php

namespace App\model;
use Eloquent; // ******** This Line *********
use Illuminate\Database\Eloquent\Model;

class HazardTable extends Model 
{
    //
    protected $table = 'hazard';
    public $timestamps = false;

}
