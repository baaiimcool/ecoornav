<?php

namespace App\model;
use Eloquent; // ******** This Line *********
use Illuminate\Database\Eloquent\Model;

class Document extends Model 
{
    //
    protected $table = 'dokumen';
    public $timestamps = false;

}
